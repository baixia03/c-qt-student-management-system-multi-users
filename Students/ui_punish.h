/********************************************************************************
** Form generated from reading UI file 'punish.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PUNISH_H
#define UI_PUNISH_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Punish
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QComboBox *comboBox_semester;
    QLabel *label_5;
    QLabel *label_3;
    QLineEdit *lineEdit_handling;
    QLabel *label_4;
    QComboBox *comboBox_year;
    QLabel *label_2;
    QLabel *label;
    QLineEdit *lineEdit_reason;
    QPushButton *pushButton_add2;
    QPushButton *pushButton_add1;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QCheckBox *checkBox;
    QDateEdit *dateEdit;
    QLabel *label_6;
    QLineEdit *lineEdit_result;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;

    void setupUi(QDialog *Punish)
    {
        if (Punish->objectName().isEmpty())
            Punish->setObjectName(QString::fromUtf8("Punish"));
        Punish->resize(339, 241);
        verticalLayout = new QVBoxLayout(Punish);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(-1, -1, 0, -1);
        comboBox_semester = new QComboBox(Punish);
        comboBox_semester->setObjectName(QString::fromUtf8("comboBox_semester"));

        gridLayout->addWidget(comboBox_semester, 3, 1, 1, 1);

        label_5 = new QLabel(Punish);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 4, 0, 1, 1, Qt::AlignHCenter);

        label_3 = new QLabel(Punish);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1, Qt::AlignHCenter);

        lineEdit_handling = new QLineEdit(Punish);
        lineEdit_handling->setObjectName(QString::fromUtf8("lineEdit_handling"));

        gridLayout->addWidget(lineEdit_handling, 1, 1, 1, 1);

        label_4 = new QLabel(Punish);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1, Qt::AlignHCenter);

        comboBox_year = new QComboBox(Punish);
        comboBox_year->setObjectName(QString::fromUtf8("comboBox_year"));

        gridLayout->addWidget(comboBox_year, 4, 1, 1, 1);

        label_2 = new QLabel(Punish);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1, Qt::AlignHCenter);

        label = new QLabel(Punish);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1, Qt::AlignHCenter);

        lineEdit_reason = new QLineEdit(Punish);
        lineEdit_reason->setObjectName(QString::fromUtf8("lineEdit_reason"));

        gridLayout->addWidget(lineEdit_reason, 0, 1, 1, 1);

        pushButton_add2 = new QPushButton(Punish);
        pushButton_add2->setObjectName(QString::fromUtf8("pushButton_add2"));
        pushButton_add2->setMinimumSize(QSize(25, 25));
        pushButton_add2->setMaximumSize(QSize(25, 25));
        pushButton_add2->setStyleSheet(QString::fromUtf8("#pushButton_add2{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add2:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add2, 4, 2, 1, 1);

        pushButton_add1 = new QPushButton(Punish);
        pushButton_add1->setObjectName(QString::fromUtf8("pushButton_add1"));
        pushButton_add1->setMinimumSize(QSize(25, 25));
        pushButton_add1->setMaximumSize(QSize(25, 25));
        pushButton_add1->setStyleSheet(QString::fromUtf8("#pushButton_add1{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add1:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add1, 3, 2, 1, 1);

        widget = new QWidget(Punish);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setLayoutDirection(Qt::LeftToRight);
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(3);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        checkBox = new QCheckBox(widget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        horizontalLayout->addWidget(checkBox);

        dateEdit = new QDateEdit(widget);
        dateEdit->setObjectName(QString::fromUtf8("dateEdit"));

        horizontalLayout->addWidget(dateEdit);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 7);

        gridLayout->addWidget(widget, 2, 1, 1, 1);

        label_6 = new QLabel(Punish);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 5, 0, 1, 1, Qt::AlignHCenter);

        lineEdit_result = new QLineEdit(Punish);
        lineEdit_result->setObjectName(QString::fromUtf8("lineEdit_result"));

        gridLayout->addWidget(lineEdit_result, 5, 1, 1, 1);

        gridLayout->setRowStretch(0, 1);
        gridLayout->setColumnStretch(0, 2);

        verticalLayout->addLayout(gridLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        pushButton = new QPushButton(Punish);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(Punish);

        QMetaObject::connectSlotsByName(Punish);
    } // setupUi

    void retranslateUi(QDialog *Punish)
    {
        Punish->setWindowTitle(QCoreApplication::translate("Punish", "Dialog", nullptr));
        label_5->setText(QCoreApplication::translate("Punish", "\345\271\264\344\273\275", nullptr));
        label_3->setText(QCoreApplication::translate("Punish", "\346\227\245\346\234\237", nullptr));
        label_4->setText(QCoreApplication::translate("Punish", "\345\255\246\346\234\237", nullptr));
        label_2->setText(QCoreApplication::translate("Punish", "\345\244\204\347\220\206", nullptr));
        label->setText(QCoreApplication::translate("Punish", "\345\216\237\345\233\240", nullptr));
        pushButton_add2->setText(QString());
        pushButton_add1->setText(QString());
        checkBox->setText(QString());
        label_6->setText(QCoreApplication::translate("Punish", "\347\273\223\346\236\234", nullptr));
        pushButton->setText(QCoreApplication::translate("Punish", "\347\241\256\350\256\244", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Punish: public Ui_Punish {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PUNISH_H
