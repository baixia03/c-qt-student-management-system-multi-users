/********************************************************************************
** Form generated from reading UI file 'user.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USER_H
#define UI_USER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_User
{
public:
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit_username;
    QLineEdit *lineEdit_password;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QCheckBox *checkBox_import;
    QCheckBox *checkBox_admin;
    QCheckBox *checkBox_read;
    QCheckBox *checkBox_delete;
    QCheckBox *checkBox_search;
    QCheckBox *checkBox_write;
    QCheckBox *checkBox_print;
    QFrame *line;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_save;
    QPushButton *pushButton_close;

    void setupUi(QDialog *User)
    {
        if (User->objectName().isEmpty())
            User->setObjectName(QString::fromUtf8("User"));
        User->resize(369, 266);
        verticalLayout = new QVBoxLayout(User);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setHorizontalSpacing(25);
        formLayout->setVerticalSpacing(7);
        formLayout->setContentsMargins(17, -1, 27, -1);
        label = new QLabel(User);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(User);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        lineEdit_username = new QLineEdit(User);
        lineEdit_username->setObjectName(QString::fromUtf8("lineEdit_username"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit_username);

        lineEdit_password = new QLineEdit(User);
        lineEdit_password->setObjectName(QString::fromUtf8("lineEdit_password"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lineEdit_password);


        verticalLayout->addLayout(formLayout);

        groupBox = new QGroupBox(User);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        checkBox_import = new QCheckBox(groupBox);
        checkBox_import->setObjectName(QString::fromUtf8("checkBox_import"));

        gridLayout->addWidget(checkBox_import, 1, 2, 1, 1);

        checkBox_admin = new QCheckBox(groupBox);
        checkBox_admin->setObjectName(QString::fromUtf8("checkBox_admin"));

        gridLayout->addWidget(checkBox_admin, 0, 0, 1, 1);

        checkBox_read = new QCheckBox(groupBox);
        checkBox_read->setObjectName(QString::fromUtf8("checkBox_read"));

        gridLayout->addWidget(checkBox_read, 0, 1, 1, 1);

        checkBox_delete = new QCheckBox(groupBox);
        checkBox_delete->setObjectName(QString::fromUtf8("checkBox_delete"));

        gridLayout->addWidget(checkBox_delete, 1, 1, 1, 1);

        checkBox_search = new QCheckBox(groupBox);
        checkBox_search->setObjectName(QString::fromUtf8("checkBox_search"));

        gridLayout->addWidget(checkBox_search, 1, 0, 1, 1);

        checkBox_write = new QCheckBox(groupBox);
        checkBox_write->setObjectName(QString::fromUtf8("checkBox_write"));

        gridLayout->addWidget(checkBox_write, 0, 2, 1, 1);

        checkBox_print = new QCheckBox(groupBox);
        checkBox_print->setObjectName(QString::fromUtf8("checkBox_print"));

        gridLayout->addWidget(checkBox_print, 2, 0, 1, 1);


        verticalLayout->addWidget(groupBox);

        line = new QFrame(User);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton_save = new QPushButton(User);
        pushButton_save->setObjectName(QString::fromUtf8("pushButton_save"));

        horizontalLayout->addWidget(pushButton_save);

        pushButton_close = new QPushButton(User);
        pushButton_close->setObjectName(QString::fromUtf8("pushButton_close"));

        horizontalLayout->addWidget(pushButton_close);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(User);

        QMetaObject::connectSlotsByName(User);
    } // setupUi

    void retranslateUi(QDialog *User)
    {
        User->setWindowTitle(QCoreApplication::translate("User", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("User", "\347\224\250\346\210\267", nullptr));
        label_2->setText(QCoreApplication::translate("User", "\345\257\206\347\240\201", nullptr));
        groupBox->setTitle(QCoreApplication::translate("User", "\346\235\203\351\231\220", nullptr));
        checkBox_import->setText(QCoreApplication::translate("User", "\345\257\274\345\205\245", nullptr));
        checkBox_admin->setText(QCoreApplication::translate("User", "\347\256\241\347\220\206\345\221\230", nullptr));
        checkBox_read->setText(QCoreApplication::translate("User", "\350\257\273", nullptr));
        checkBox_delete->setText(QCoreApplication::translate("User", "\345\210\240\351\231\244", nullptr));
        checkBox_search->setText(QCoreApplication::translate("User", "\346\237\245\346\211\276", nullptr));
        checkBox_write->setText(QCoreApplication::translate("User", "\345\206\231", nullptr));
        checkBox_print->setText(QCoreApplication::translate("User", "\346\211\223\345\215\260", nullptr));
        pushButton_save->setText(QCoreApplication::translate("User", "\344\277\235\345\255\230", nullptr));
        pushButton_close->setText(QCoreApplication::translate("User", "\345\205\263\351\227\255", nullptr));
    } // retranslateUi

};

namespace Ui {
    class User: public Ui_User {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USER_H
