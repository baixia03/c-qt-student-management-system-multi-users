/********************************************************************************
** Form generated from reading UI file 'studentinfo.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STUDENTINFO_H
#define UI_STUDENTINFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StudentInfo
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_7;
    QFormLayout *formLayout;
    QLabel *label_2;
    QLineEdit *lineEdit_name;
    QLabel *label_4;
    QLineEdit *lineEdit_number;
    QLabel *label_5;
    QDateEdit *dateEdit_birthData;
    QLabel *label_22;
    QLineEdit *lineEdit_birthPlace;
    QLabel *label_8;
    QLabel *label_10;
    QLabel *label_9;
    QLabel *label_12;
    QLabel *label_11;
    QLabel *label_7;
    QComboBox *comboBox_status;
    QLabel *label_6;
    QComboBox *comboBox_sex;
    QLabel *label;
    QLineEdit *lineEdit_idCard;
    QLabel *label_3;
    QComboBox *comboBox_university;
    QComboBox *comboBox_college;
    QComboBox *comboBox_profession;
    QComboBox *comboBox_province;
    QComboBox *comboBox_city;
    QComboBox *comboBox_nation;
    QLabel *label_14;
    QComboBox *comboBox_political;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_13;
    QDateEdit *dateEdit_dataOfAdmission;
    QLabel *label_16;
    QDateEdit *dateEdit_dataOfGraduation;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_15;
    QSpacerItem *horizontalSpacer;
    QLabel *label_17;
    QLineEdit *lineEdit_phone;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *lineEdit_homeAddress;
    QLabel *label_18;
    QComboBox *comboBox_socialStatus;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QTableWidget *tableWidget_grade;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_addGrade;
    QPushButton *pushButton_editGrade;
    QPushButton *pushButton_deleteGrade;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_exportExam;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_4;
    QTableWidget *tableWidget_honor;
    QHBoxLayout *horizontalLayout_12;
    QPushButton *pushButton_addHonor;
    QPushButton *pushButton_editHonor;
    QPushButton *pushButton_deleteHonor;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButton_exportHonor;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_9;
    QTableWidget *tableWidget_debt;
    QHBoxLayout *horizontalLayout_13;
    QPushButton *pushButton_addDebt;
    QPushButton *pushButton_editDebt;
    QPushButton *pushButton_deleteDebt;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton_exportDebt;
    QWidget *tab_4;
    QVBoxLayout *verticalLayout_10;
    QTableWidget *tableWidget_scholarShip;
    QHBoxLayout *horizontalLayout_14;
    QPushButton *pushButton_addScholarShip;
    QPushButton *pushButton_editScholarShip;
    QPushButton *pushButton_deleteScholarShip;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_exportScholarShip;
    QWidget *tab_5;
    QVBoxLayout *verticalLayout_11;
    QTableWidget *tableWidget_punish;
    QHBoxLayout *horizontalLayout_15;
    QPushButton *pushButton_addPunish;
    QPushButton *pushButton_editPunish;
    QPushButton *pushButton_deletePunish;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *pushButton_exportPunish;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_19;
    QComboBox *comboBox_blood;
    QLabel *label_20;
    QComboBox *comboBox_eye;
    QLabel *label_21;
    QComboBox *comboBox_skin;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_10;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_23;
    QLineEdit *lineEdit_fatherName;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_2;
    QCheckBox *checkBox_father_workToTheState;
    QCheckBox *checkBox_father_selfEmployed;
    QLabel *label_24;
    QLineEdit *lineEdit_motherName;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout_8;
    QCheckBox *checkBox_mother_workToTheState;
    QCheckBox *checkBox_mother_selfEmployed;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_25;
    QTextEdit *textEdit_parentsOtherInformation;
    QGroupBox *groupBox_file;
    QGridLayout *gridLayout_2;
    QPushButton *pushButton_openFile1;
    QPushButton *pushButton_openFile3;
    QLineEdit *lineEdit_file1;
    QPushButton *pushButton_openFile4;
    QPushButton *pushButton_saveFile1;
    QPushButton *pushButton_openFile2;
    QLineEdit *lineEdit_file2;
    QLineEdit *lineEdit_file3;
    QLineEdit *lineEdit_file4;
    QPushButton *pushButton_saveFile2;
    QPushButton *pushButton_saveFile3;
    QPushButton *pushButton_saveFile4;
    QVBoxLayout *verticalLayout_8;
    QFrame *frame_photo;
    QPushButton *pushButton_openPhoto;
    QPushButton *pushButton_cancelPhoto;
    QLabel *label_27;
    QLabel *label_28;
    QTextEdit *textEdit_otherInterest;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *pushButton_dataDictionary;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton_save;
    QPushButton *pushButton_exit;

    void setupUi(QDialog *StudentInfo)
    {
        if (StudentInfo->objectName().isEmpty())
            StudentInfo->setObjectName(QString::fromUtf8("StudentInfo"));
        StudentInfo->resize(1143, 651);
        gridLayout = new QGridLayout(StudentInfo);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout->setLabelAlignment(Qt::AlignCenter);
        formLayout->setFormAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_2 = new QLabel(StudentInfo);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        lineEdit_name = new QLineEdit(StudentInfo);
        lineEdit_name->setObjectName(QString::fromUtf8("lineEdit_name"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit_name);

        label_4 = new QLabel(StudentInfo);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_4);

        lineEdit_number = new QLineEdit(StudentInfo);
        lineEdit_number->setObjectName(QString::fromUtf8("lineEdit_number"));

        formLayout->setWidget(1, QFormLayout::FieldRole, lineEdit_number);

        label_5 = new QLabel(StudentInfo);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_5);

        dateEdit_birthData = new QDateEdit(StudentInfo);
        dateEdit_birthData->setObjectName(QString::fromUtf8("dateEdit_birthData"));

        formLayout->setWidget(5, QFormLayout::FieldRole, dateEdit_birthData);

        label_22 = new QLabel(StudentInfo);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_22);

        lineEdit_birthPlace = new QLineEdit(StudentInfo);
        lineEdit_birthPlace->setObjectName(QString::fromUtf8("lineEdit_birthPlace"));

        formLayout->setWidget(6, QFormLayout::FieldRole, lineEdit_birthPlace);

        label_8 = new QLabel(StudentInfo);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        formLayout->setWidget(8, QFormLayout::LabelRole, label_8);

        label_10 = new QLabel(StudentInfo);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        formLayout->setWidget(9, QFormLayout::LabelRole, label_10);

        label_9 = new QLabel(StudentInfo);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout->setWidget(10, QFormLayout::LabelRole, label_9);

        label_12 = new QLabel(StudentInfo);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        formLayout->setWidget(11, QFormLayout::LabelRole, label_12);

        label_11 = new QLabel(StudentInfo);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        formLayout->setWidget(12, QFormLayout::LabelRole, label_11);

        label_7 = new QLabel(StudentInfo);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout->setWidget(13, QFormLayout::LabelRole, label_7);

        comboBox_status = new QComboBox(StudentInfo);
        comboBox_status->setObjectName(QString::fromUtf8("comboBox_status"));

        formLayout->setWidget(13, QFormLayout::FieldRole, comboBox_status);

        label_6 = new QLabel(StudentInfo);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_6);

        comboBox_sex = new QComboBox(StudentInfo);
        comboBox_sex->addItem(QString());
        comboBox_sex->addItem(QString());
        comboBox_sex->setObjectName(QString::fromUtf8("comboBox_sex"));

        formLayout->setWidget(2, QFormLayout::FieldRole, comboBox_sex);

        label = new QLabel(StudentInfo);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(7, QFormLayout::LabelRole, label);

        lineEdit_idCard = new QLineEdit(StudentInfo);
        lineEdit_idCard->setObjectName(QString::fromUtf8("lineEdit_idCard"));

        formLayout->setWidget(7, QFormLayout::FieldRole, lineEdit_idCard);

        label_3 = new QLabel(StudentInfo);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        comboBox_university = new QComboBox(StudentInfo);
        comboBox_university->setObjectName(QString::fromUtf8("comboBox_university"));

        formLayout->setWidget(10, QFormLayout::FieldRole, comboBox_university);

        comboBox_college = new QComboBox(StudentInfo);
        comboBox_college->setObjectName(QString::fromUtf8("comboBox_college"));

        formLayout->setWidget(11, QFormLayout::FieldRole, comboBox_college);

        comboBox_profession = new QComboBox(StudentInfo);
        comboBox_profession->setObjectName(QString::fromUtf8("comboBox_profession"));

        formLayout->setWidget(12, QFormLayout::FieldRole, comboBox_profession);

        comboBox_province = new QComboBox(StudentInfo);
        comboBox_province->setObjectName(QString::fromUtf8("comboBox_province"));

        formLayout->setWidget(8, QFormLayout::FieldRole, comboBox_province);

        comboBox_city = new QComboBox(StudentInfo);
        comboBox_city->setObjectName(QString::fromUtf8("comboBox_city"));

        formLayout->setWidget(9, QFormLayout::FieldRole, comboBox_city);

        comboBox_nation = new QComboBox(StudentInfo);
        comboBox_nation->setObjectName(QString::fromUtf8("comboBox_nation"));

        formLayout->setWidget(3, QFormLayout::FieldRole, comboBox_nation);

        label_14 = new QLabel(StudentInfo);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_14);

        comboBox_political = new QComboBox(StudentInfo);
        comboBox_political->setObjectName(QString::fromUtf8("comboBox_political"));

        formLayout->setWidget(4, QFormLayout::FieldRole, comboBox_political);


        horizontalLayout_7->addLayout(formLayout);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_13 = new QLabel(StudentInfo);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout->addWidget(label_13, 0, Qt::AlignHCenter);

        dateEdit_dataOfAdmission = new QDateEdit(StudentInfo);
        dateEdit_dataOfAdmission->setObjectName(QString::fromUtf8("dateEdit_dataOfAdmission"));

        horizontalLayout->addWidget(dateEdit_dataOfAdmission);

        label_16 = new QLabel(StudentInfo);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        horizontalLayout->addWidget(label_16, 0, Qt::AlignHCenter);

        dateEdit_dataOfGraduation = new QDateEdit(StudentInfo);
        dateEdit_dataOfGraduation->setObjectName(QString::fromUtf8("dateEdit_dataOfGraduation"));

        horizontalLayout->addWidget(dateEdit_dataOfGraduation);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_15 = new QLabel(StudentInfo);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setAutoFillBackground(false);
        label_15->setScaledContents(false);
        label_15->setWordWrap(false);
        label_15->setOpenExternalLinks(false);

        horizontalLayout_3->addWidget(label_15);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        label_17 = new QLabel(StudentInfo);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_3->addWidget(label_17);

        lineEdit_phone = new QLineEdit(StudentInfo);
        lineEdit_phone->setObjectName(QString::fromUtf8("lineEdit_phone"));

        horizontalLayout_3->addWidget(lineEdit_phone);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lineEdit_homeAddress = new QLineEdit(StudentInfo);
        lineEdit_homeAddress->setObjectName(QString::fromUtf8("lineEdit_homeAddress"));

        horizontalLayout_4->addWidget(lineEdit_homeAddress);

        label_18 = new QLabel(StudentInfo);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_4->addWidget(label_18);

        comboBox_socialStatus = new QComboBox(StudentInfo);
        comboBox_socialStatus->setObjectName(QString::fromUtf8("comboBox_socialStatus"));

        horizontalLayout_4->addWidget(comboBox_socialStatus);


        verticalLayout->addLayout(horizontalLayout_4);


        verticalLayout_3->addLayout(verticalLayout);

        tabWidget = new QTabWidget(StudentInfo);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        tableWidget_grade = new QTableWidget(tab);
        if (tableWidget_grade->columnCount() < 5)
            tableWidget_grade->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget_grade->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget_grade->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget_grade->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget_grade->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget_grade->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        tableWidget_grade->setObjectName(QString::fromUtf8("tableWidget_grade"));
        tableWidget_grade->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout_2->addWidget(tableWidget_grade);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButton_addGrade = new QPushButton(tab);
        pushButton_addGrade->setObjectName(QString::fromUtf8("pushButton_addGrade"));

        horizontalLayout_5->addWidget(pushButton_addGrade);

        pushButton_editGrade = new QPushButton(tab);
        pushButton_editGrade->setObjectName(QString::fromUtf8("pushButton_editGrade"));

        horizontalLayout_5->addWidget(pushButton_editGrade);

        pushButton_deleteGrade = new QPushButton(tab);
        pushButton_deleteGrade->setObjectName(QString::fromUtf8("pushButton_deleteGrade"));

        horizontalLayout_5->addWidget(pushButton_deleteGrade);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        pushButton_exportExam = new QPushButton(tab);
        pushButton_exportExam->setObjectName(QString::fromUtf8("pushButton_exportExam"));

        horizontalLayout_5->addWidget(pushButton_exportExam);


        verticalLayout_2->addLayout(horizontalLayout_5);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_4 = new QVBoxLayout(tab_2);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        tableWidget_honor = new QTableWidget(tab_2);
        if (tableWidget_honor->columnCount() < 5)
            tableWidget_honor->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget_honor->setHorizontalHeaderItem(0, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget_honor->setHorizontalHeaderItem(1, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget_honor->setHorizontalHeaderItem(2, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget_honor->setHorizontalHeaderItem(3, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget_honor->setHorizontalHeaderItem(4, __qtablewidgetitem9);
        tableWidget_honor->setObjectName(QString::fromUtf8("tableWidget_honor"));
        tableWidget_honor->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout_4->addWidget(tableWidget_honor);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        pushButton_addHonor = new QPushButton(tab_2);
        pushButton_addHonor->setObjectName(QString::fromUtf8("pushButton_addHonor"));

        horizontalLayout_12->addWidget(pushButton_addHonor);

        pushButton_editHonor = new QPushButton(tab_2);
        pushButton_editHonor->setObjectName(QString::fromUtf8("pushButton_editHonor"));

        horizontalLayout_12->addWidget(pushButton_editHonor);

        pushButton_deleteHonor = new QPushButton(tab_2);
        pushButton_deleteHonor->setObjectName(QString::fromUtf8("pushButton_deleteHonor"));

        horizontalLayout_12->addWidget(pushButton_deleteHonor);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_4);

        pushButton_exportHonor = new QPushButton(tab_2);
        pushButton_exportHonor->setObjectName(QString::fromUtf8("pushButton_exportHonor"));

        horizontalLayout_12->addWidget(pushButton_exportHonor);


        verticalLayout_4->addLayout(horizontalLayout_12);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        verticalLayout_9 = new QVBoxLayout(tab_3);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        tableWidget_debt = new QTableWidget(tab_3);
        if (tableWidget_debt->columnCount() < 6)
            tableWidget_debt->setColumnCount(6);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget_debt->setHorizontalHeaderItem(0, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget_debt->setHorizontalHeaderItem(1, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget_debt->setHorizontalHeaderItem(2, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableWidget_debt->setHorizontalHeaderItem(3, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tableWidget_debt->setHorizontalHeaderItem(4, __qtablewidgetitem14);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableWidget_debt->setHorizontalHeaderItem(5, __qtablewidgetitem15);
        tableWidget_debt->setObjectName(QString::fromUtf8("tableWidget_debt"));
        tableWidget_debt->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout_9->addWidget(tableWidget_debt);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        pushButton_addDebt = new QPushButton(tab_3);
        pushButton_addDebt->setObjectName(QString::fromUtf8("pushButton_addDebt"));

        horizontalLayout_13->addWidget(pushButton_addDebt);

        pushButton_editDebt = new QPushButton(tab_3);
        pushButton_editDebt->setObjectName(QString::fromUtf8("pushButton_editDebt"));

        horizontalLayout_13->addWidget(pushButton_editDebt);

        pushButton_deleteDebt = new QPushButton(tab_3);
        pushButton_deleteDebt->setObjectName(QString::fromUtf8("pushButton_deleteDebt"));

        horizontalLayout_13->addWidget(pushButton_deleteDebt);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_5);

        pushButton_exportDebt = new QPushButton(tab_3);
        pushButton_exportDebt->setObjectName(QString::fromUtf8("pushButton_exportDebt"));

        horizontalLayout_13->addWidget(pushButton_exportDebt);


        verticalLayout_9->addLayout(horizontalLayout_13);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        verticalLayout_10 = new QVBoxLayout(tab_4);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        tableWidget_scholarShip = new QTableWidget(tab_4);
        if (tableWidget_scholarShip->columnCount() < 4)
            tableWidget_scholarShip->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        tableWidget_scholarShip->setHorizontalHeaderItem(0, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        tableWidget_scholarShip->setHorizontalHeaderItem(1, __qtablewidgetitem17);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        tableWidget_scholarShip->setHorizontalHeaderItem(2, __qtablewidgetitem18);
        QTableWidgetItem *__qtablewidgetitem19 = new QTableWidgetItem();
        tableWidget_scholarShip->setHorizontalHeaderItem(3, __qtablewidgetitem19);
        tableWidget_scholarShip->setObjectName(QString::fromUtf8("tableWidget_scholarShip"));
        tableWidget_scholarShip->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout_10->addWidget(tableWidget_scholarShip);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        pushButton_addScholarShip = new QPushButton(tab_4);
        pushButton_addScholarShip->setObjectName(QString::fromUtf8("pushButton_addScholarShip"));

        horizontalLayout_14->addWidget(pushButton_addScholarShip);

        pushButton_editScholarShip = new QPushButton(tab_4);
        pushButton_editScholarShip->setObjectName(QString::fromUtf8("pushButton_editScholarShip"));

        horizontalLayout_14->addWidget(pushButton_editScholarShip);

        pushButton_deleteScholarShip = new QPushButton(tab_4);
        pushButton_deleteScholarShip->setObjectName(QString::fromUtf8("pushButton_deleteScholarShip"));

        horizontalLayout_14->addWidget(pushButton_deleteScholarShip);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_6);

        pushButton_exportScholarShip = new QPushButton(tab_4);
        pushButton_exportScholarShip->setObjectName(QString::fromUtf8("pushButton_exportScholarShip"));

        horizontalLayout_14->addWidget(pushButton_exportScholarShip);


        verticalLayout_10->addLayout(horizontalLayout_14);

        tabWidget->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        verticalLayout_11 = new QVBoxLayout(tab_5);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        tableWidget_punish = new QTableWidget(tab_5);
        if (tableWidget_punish->columnCount() < 6)
            tableWidget_punish->setColumnCount(6);
        QTableWidgetItem *__qtablewidgetitem20 = new QTableWidgetItem();
        tableWidget_punish->setHorizontalHeaderItem(0, __qtablewidgetitem20);
        QTableWidgetItem *__qtablewidgetitem21 = new QTableWidgetItem();
        tableWidget_punish->setHorizontalHeaderItem(1, __qtablewidgetitem21);
        QTableWidgetItem *__qtablewidgetitem22 = new QTableWidgetItem();
        tableWidget_punish->setHorizontalHeaderItem(2, __qtablewidgetitem22);
        QTableWidgetItem *__qtablewidgetitem23 = new QTableWidgetItem();
        tableWidget_punish->setHorizontalHeaderItem(3, __qtablewidgetitem23);
        QTableWidgetItem *__qtablewidgetitem24 = new QTableWidgetItem();
        tableWidget_punish->setHorizontalHeaderItem(4, __qtablewidgetitem24);
        QTableWidgetItem *__qtablewidgetitem25 = new QTableWidgetItem();
        tableWidget_punish->setHorizontalHeaderItem(5, __qtablewidgetitem25);
        tableWidget_punish->setObjectName(QString::fromUtf8("tableWidget_punish"));
        tableWidget_punish->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout_11->addWidget(tableWidget_punish);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        pushButton_addPunish = new QPushButton(tab_5);
        pushButton_addPunish->setObjectName(QString::fromUtf8("pushButton_addPunish"));

        horizontalLayout_15->addWidget(pushButton_addPunish);

        pushButton_editPunish = new QPushButton(tab_5);
        pushButton_editPunish->setObjectName(QString::fromUtf8("pushButton_editPunish"));

        horizontalLayout_15->addWidget(pushButton_editPunish);

        pushButton_deletePunish = new QPushButton(tab_5);
        pushButton_deletePunish->setObjectName(QString::fromUtf8("pushButton_deletePunish"));

        horizontalLayout_15->addWidget(pushButton_deletePunish);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_7);

        pushButton_exportPunish = new QPushButton(tab_5);
        pushButton_exportPunish->setObjectName(QString::fromUtf8("pushButton_exportPunish"));

        horizontalLayout_15->addWidget(pushButton_exportPunish);


        verticalLayout_11->addLayout(horizontalLayout_15);

        tabWidget->addTab(tab_5, QString());

        verticalLayout_3->addWidget(tabWidget);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_19 = new QLabel(StudentInfo);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        horizontalLayout_6->addWidget(label_19);

        comboBox_blood = new QComboBox(StudentInfo);
        comboBox_blood->setObjectName(QString::fromUtf8("comboBox_blood"));

        horizontalLayout_6->addWidget(comboBox_blood);

        label_20 = new QLabel(StudentInfo);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        horizontalLayout_6->addWidget(label_20);

        comboBox_eye = new QComboBox(StudentInfo);
        comboBox_eye->setObjectName(QString::fromUtf8("comboBox_eye"));

        horizontalLayout_6->addWidget(comboBox_eye);

        label_21 = new QLabel(StudentInfo);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        horizontalLayout_6->addWidget(label_21);

        comboBox_skin = new QComboBox(StudentInfo);
        comboBox_skin->setObjectName(QString::fromUtf8("comboBox_skin"));

        horizontalLayout_6->addWidget(comboBox_skin);


        verticalLayout_3->addLayout(horizontalLayout_6);


        horizontalLayout_7->addLayout(verticalLayout_3);

        horizontalLayout_7->setStretch(0, 1);
        horizontalLayout_7->setStretch(1, 3);

        verticalLayout_5->addLayout(horizontalLayout_7);

        groupBox = new QGroupBox(StudentInfo);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        horizontalLayout_10 = new QHBoxLayout(groupBox);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_23 = new QLabel(groupBox);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        verticalLayout_6->addWidget(label_23);

        lineEdit_fatherName = new QLineEdit(groupBox);
        lineEdit_fatherName->setObjectName(QString::fromUtf8("lineEdit_fatherName"));

        verticalLayout_6->addWidget(lineEdit_fatherName);

        widget = new QWidget(groupBox);
        widget->setObjectName(QString::fromUtf8("widget"));
        horizontalLayout_2 = new QHBoxLayout(widget);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        checkBox_father_workToTheState = new QCheckBox(widget);
        checkBox_father_workToTheState->setObjectName(QString::fromUtf8("checkBox_father_workToTheState"));
        checkBox_father_workToTheState->setAutoExclusive(true);

        horizontalLayout_2->addWidget(checkBox_father_workToTheState);

        checkBox_father_selfEmployed = new QCheckBox(widget);
        checkBox_father_selfEmployed->setObjectName(QString::fromUtf8("checkBox_father_selfEmployed"));
        checkBox_father_selfEmployed->setAutoExclusive(true);

        horizontalLayout_2->addWidget(checkBox_father_selfEmployed);


        verticalLayout_6->addWidget(widget);

        label_24 = new QLabel(groupBox);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        verticalLayout_6->addWidget(label_24);

        lineEdit_motherName = new QLineEdit(groupBox);
        lineEdit_motherName->setObjectName(QString::fromUtf8("lineEdit_motherName"));

        verticalLayout_6->addWidget(lineEdit_motherName);

        widget_2 = new QWidget(groupBox);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        horizontalLayout_8 = new QHBoxLayout(widget_2);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        checkBox_mother_workToTheState = new QCheckBox(widget_2);
        checkBox_mother_workToTheState->setObjectName(QString::fromUtf8("checkBox_mother_workToTheState"));
        checkBox_mother_workToTheState->setAutoExclusive(true);

        horizontalLayout_8->addWidget(checkBox_mother_workToTheState);

        checkBox_mother_selfEmployed = new QCheckBox(widget_2);
        checkBox_mother_selfEmployed->setObjectName(QString::fromUtf8("checkBox_mother_selfEmployed"));
        checkBox_mother_selfEmployed->setAutoExclusive(true);

        horizontalLayout_8->addWidget(checkBox_mother_selfEmployed);


        verticalLayout_6->addWidget(widget_2);


        horizontalLayout_10->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label_25 = new QLabel(groupBox);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        verticalLayout_7->addWidget(label_25);

        textEdit_parentsOtherInformation = new QTextEdit(groupBox);
        textEdit_parentsOtherInformation->setObjectName(QString::fromUtf8("textEdit_parentsOtherInformation"));

        verticalLayout_7->addWidget(textEdit_parentsOtherInformation);


        horizontalLayout_10->addLayout(verticalLayout_7);

        groupBox_file = new QGroupBox(groupBox);
        groupBox_file->setObjectName(QString::fromUtf8("groupBox_file"));
        groupBox_file->setMinimumSize(QSize(200, 0));
        gridLayout_2 = new QGridLayout(groupBox_file);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        pushButton_openFile1 = new QPushButton(groupBox_file);
        pushButton_openFile1->setObjectName(QString::fromUtf8("pushButton_openFile1"));
        pushButton_openFile1->setMaximumSize(QSize(50, 50));
        pushButton_openFile1->setStyleSheet(QString::fromUtf8("#pushButton_openFile1{\n"
"Border-image:url(:/images/open.png);\n"
"}\n"
"#pushButton_openFile1:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout_2->addWidget(pushButton_openFile1, 0, 0, 1, 1);

        pushButton_openFile3 = new QPushButton(groupBox_file);
        pushButton_openFile3->setObjectName(QString::fromUtf8("pushButton_openFile3"));
        pushButton_openFile3->setMaximumSize(QSize(50, 50));
        pushButton_openFile3->setStyleSheet(QString::fromUtf8("#pushButton_openFile3{\n"
"Border-image:url(:/images/open.png);\n"
"}\n"
"#pushButton_openFile3:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout_2->addWidget(pushButton_openFile3, 2, 0, 1, 1);

        lineEdit_file1 = new QLineEdit(groupBox_file);
        lineEdit_file1->setObjectName(QString::fromUtf8("lineEdit_file1"));

        gridLayout_2->addWidget(lineEdit_file1, 0, 1, 1, 1);

        pushButton_openFile4 = new QPushButton(groupBox_file);
        pushButton_openFile4->setObjectName(QString::fromUtf8("pushButton_openFile4"));
        pushButton_openFile4->setMaximumSize(QSize(50, 50));
        pushButton_openFile4->setStyleSheet(QString::fromUtf8("#pushButton_openFile4{\n"
"Border-image:url(:/images/open.png);\n"
"}\n"
"#pushButton_openFile4:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout_2->addWidget(pushButton_openFile4, 3, 0, 1, 1);

        pushButton_saveFile1 = new QPushButton(groupBox_file);
        pushButton_saveFile1->setObjectName(QString::fromUtf8("pushButton_saveFile1"));
        pushButton_saveFile1->setMaximumSize(QSize(50, 50));
        pushButton_saveFile1->setStyleSheet(QString::fromUtf8("#pushButton_saveFile1{\n"
"Border-image:url(:/images/save.png);\n"
"}\n"
"#pushButton_saveFile1:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout_2->addWidget(pushButton_saveFile1, 0, 2, 1, 1);

        pushButton_openFile2 = new QPushButton(groupBox_file);
        pushButton_openFile2->setObjectName(QString::fromUtf8("pushButton_openFile2"));
        pushButton_openFile2->setMaximumSize(QSize(50, 50));
        pushButton_openFile2->setStyleSheet(QString::fromUtf8("#pushButton_openFile2{\n"
"Border-image:url(:/images/open.png);\n"
"}\n"
"#pushButton_openFile2:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout_2->addWidget(pushButton_openFile2, 1, 0, 1, 1);

        lineEdit_file2 = new QLineEdit(groupBox_file);
        lineEdit_file2->setObjectName(QString::fromUtf8("lineEdit_file2"));

        gridLayout_2->addWidget(lineEdit_file2, 1, 1, 1, 1);

        lineEdit_file3 = new QLineEdit(groupBox_file);
        lineEdit_file3->setObjectName(QString::fromUtf8("lineEdit_file3"));

        gridLayout_2->addWidget(lineEdit_file3, 2, 1, 1, 1);

        lineEdit_file4 = new QLineEdit(groupBox_file);
        lineEdit_file4->setObjectName(QString::fromUtf8("lineEdit_file4"));

        gridLayout_2->addWidget(lineEdit_file4, 3, 1, 1, 1);

        pushButton_saveFile2 = new QPushButton(groupBox_file);
        pushButton_saveFile2->setObjectName(QString::fromUtf8("pushButton_saveFile2"));
        pushButton_saveFile2->setMaximumSize(QSize(50, 50));
        pushButton_saveFile2->setStyleSheet(QString::fromUtf8("#pushButton_saveFile2{\n"
"Border-image:url(:/images/save.png);\n"
"}\n"
"#pushButton_saveFile2:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout_2->addWidget(pushButton_saveFile2, 1, 2, 1, 1);

        pushButton_saveFile3 = new QPushButton(groupBox_file);
        pushButton_saveFile3->setObjectName(QString::fromUtf8("pushButton_saveFile3"));
        pushButton_saveFile3->setMaximumSize(QSize(50, 50));
        pushButton_saveFile3->setStyleSheet(QString::fromUtf8("#pushButton_saveFile3{\n"
"Border-image:url(:/images/save.png);\n"
"}\n"
"#pushButton_saveFile3:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout_2->addWidget(pushButton_saveFile3, 2, 2, 1, 1);

        pushButton_saveFile4 = new QPushButton(groupBox_file);
        pushButton_saveFile4->setObjectName(QString::fromUtf8("pushButton_saveFile4"));
        pushButton_saveFile4->setMaximumSize(QSize(50, 50));
        pushButton_saveFile4->setStyleSheet(QString::fromUtf8("#pushButton_saveFile4{\n"
"Border-image:url(:/images/save.png);\n"
"}\n"
"#pushButton_saveFile4:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout_2->addWidget(pushButton_saveFile4, 3, 2, 1, 1);


        horizontalLayout_10->addWidget(groupBox_file);


        verticalLayout_5->addWidget(groupBox);


        gridLayout->addLayout(verticalLayout_5, 0, 0, 1, 1);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(10);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(0, 0, 0, 0);
        frame_photo = new QFrame(StudentInfo);
        frame_photo->setObjectName(QString::fromUtf8("frame_photo"));
        frame_photo->setMinimumSize(QSize(0, 0));
        frame_photo->setMaximumSize(QSize(16777215, 16777215));
        frame_photo->setFrameShape(QFrame::Box);
        frame_photo->setFrameShadow(QFrame::Raised);
        pushButton_openPhoto = new QPushButton(frame_photo);
        pushButton_openPhoto->setObjectName(QString::fromUtf8("pushButton_openPhoto"));
        pushButton_openPhoto->setEnabled(true);
        pushButton_openPhoto->setGeometry(QRect(90, 0, 41, 31));
        pushButton_openPhoto->setStyleSheet(QString::fromUtf8("#pushButton_openPhoto{\n"
"Border-image:url(:/images/open.png);\n"
"}\n"
"#pushButton_openPhoto:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}\n"
"#pushButton_openPhoto:default{\n"
"\n"
"}\n"
"#pushButton_openPhoto:pressed{\n"
"\n"
"}"));
        pushButton_cancelPhoto = new QPushButton(frame_photo);
        pushButton_cancelPhoto->setObjectName(QString::fromUtf8("pushButton_cancelPhoto"));
        pushButton_cancelPhoto->setGeometry(QRect(130, 0, 41, 31));
        pushButton_cancelPhoto->setStyleSheet(QString::fromUtf8("#pushButton_cancelPhoto{\n"
"Border-image:url(:/images/cancel.png);\n"
"}\n"
"#pushButton_cancelPhoto:hover{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        verticalLayout_8->addWidget(frame_photo);

        label_27 = new QLabel(StudentInfo);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        verticalLayout_8->addWidget(label_27, 0, Qt::AlignHCenter);

        label_28 = new QLabel(StudentInfo);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        verticalLayout_8->addWidget(label_28, 0, Qt::AlignHCenter);

        textEdit_otherInterest = new QTextEdit(StudentInfo);
        textEdit_otherInterest->setObjectName(QString::fromUtf8("textEdit_otherInterest"));
        textEdit_otherInterest->setMaximumSize(QSize(16777215, 16777215));

        verticalLayout_8->addWidget(textEdit_otherInterest);

        verticalLayout_8->setStretch(0, 1);
        verticalLayout_8->setStretch(3, 2);

        gridLayout->addLayout(verticalLayout_8, 0, 1, 1, 1);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        pushButton_dataDictionary = new QPushButton(StudentInfo);
        pushButton_dataDictionary->setObjectName(QString::fromUtf8("pushButton_dataDictionary"));

        horizontalLayout_9->addWidget(pushButton_dataDictionary);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_2);

        pushButton_save = new QPushButton(StudentInfo);
        pushButton_save->setObjectName(QString::fromUtf8("pushButton_save"));
        pushButton_save->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_9->addWidget(pushButton_save);

        pushButton_exit = new QPushButton(StudentInfo);
        pushButton_exit->setObjectName(QString::fromUtf8("pushButton_exit"));

        horizontalLayout_9->addWidget(pushButton_exit);


        gridLayout->addLayout(horizontalLayout_9, 1, 0, 1, 2);

        gridLayout->setColumnStretch(0, 1);

        retranslateUi(StudentInfo);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(StudentInfo);
    } // setupUi

    void retranslateUi(QDialog *StudentInfo)
    {
        StudentInfo->setWindowTitle(QCoreApplication::translate("StudentInfo", "Dialog", nullptr));
        label_2->setText(QCoreApplication::translate("StudentInfo", "\345\247\223\345\220\215", nullptr));
        label_4->setText(QCoreApplication::translate("StudentInfo", "\345\255\246\345\217\267", nullptr));
        label_5->setText(QCoreApplication::translate("StudentInfo", "\345\207\272\347\224\237\346\227\245\346\234\237", nullptr));
        label_22->setText(QCoreApplication::translate("StudentInfo", "\345\207\272\347\224\237\345\234\260\347\202\271", nullptr));
        label_8->setText(QCoreApplication::translate("StudentInfo", "\347\234\201\344\273\275", nullptr));
        label_10->setText(QCoreApplication::translate("StudentInfo", "\345\237\216\345\270\202", nullptr));
        label_9->setText(QCoreApplication::translate("StudentInfo", "\345\244\247\345\255\246", nullptr));
        label_12->setText(QCoreApplication::translate("StudentInfo", "\345\255\246\351\231\242", nullptr));
        label_11->setText(QCoreApplication::translate("StudentInfo", "\344\270\223\344\270\232", nullptr));
        label_7->setText(QCoreApplication::translate("StudentInfo", "\347\212\266\346\200\201", nullptr));
        label_6->setText(QCoreApplication::translate("StudentInfo", "\346\200\247\345\210\253", nullptr));
        comboBox_sex->setItemText(0, QCoreApplication::translate("StudentInfo", "\347\224\267", nullptr));
        comboBox_sex->setItemText(1, QCoreApplication::translate("StudentInfo", "\345\245\263", nullptr));

        comboBox_sex->setCurrentText(QCoreApplication::translate("StudentInfo", "\347\224\267", nullptr));
        label->setText(QCoreApplication::translate("StudentInfo", "\350\272\253\344\273\275\350\257\201", nullptr));
        label_3->setText(QCoreApplication::translate("StudentInfo", "\346\260\221\346\227\217", nullptr));
        label_14->setText(QCoreApplication::translate("StudentInfo", "\346\224\277\346\262\273\351\235\242\350\262\214", nullptr));
        label_13->setText(QCoreApplication::translate("StudentInfo", "\345\205\245\345\255\246\346\227\245\346\234\237", nullptr));
        label_16->setText(QCoreApplication::translate("StudentInfo", "\346\257\225\344\270\232\346\227\245\346\234\237", nullptr));
        label_15->setText(QCoreApplication::translate("StudentInfo", "\345\256\266\345\272\255\344\275\217\345\235\200", nullptr));
        label_17->setText(QCoreApplication::translate("StudentInfo", "\347\224\265\350\257\235", nullptr));
        label_18->setText(QCoreApplication::translate("StudentInfo", "\347\244\276\344\272\244\351\200\224\345\276\204", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget_grade->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("StudentInfo", "\347\247\221\347\233\256", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget_grade->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("StudentInfo", "\345\210\206\346\225\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget_grade->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("StudentInfo", "\346\227\245\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget_grade->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("StudentInfo", "\345\255\246\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget_grade->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("StudentInfo", "\345\271\264\344\273\275", nullptr));
        pushButton_addGrade->setText(QCoreApplication::translate("StudentInfo", "\346\267\273\345\212\240", nullptr));
        pushButton_editGrade->setText(QCoreApplication::translate("StudentInfo", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteGrade->setText(QCoreApplication::translate("StudentInfo", "\345\210\240\351\231\244", nullptr));
        pushButton_exportExam->setText(QCoreApplication::translate("StudentInfo", "\345\257\274\345\207\272Excel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("StudentInfo", "\346\210\220\347\273\251", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget_honor->horizontalHeaderItem(0);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("StudentInfo", "\345\220\215\347\247\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget_honor->horizontalHeaderItem(1);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("StudentInfo", "\347\272\247\345\210\253", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget_honor->horizontalHeaderItem(2);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("StudentInfo", "\346\227\245\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget_honor->horizontalHeaderItem(3);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("StudentInfo", "\345\255\246\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget_honor->horizontalHeaderItem(4);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("StudentInfo", "\345\271\264\344\273\275", nullptr));
        pushButton_addHonor->setText(QCoreApplication::translate("StudentInfo", "\346\267\273\345\212\240", nullptr));
        pushButton_editHonor->setText(QCoreApplication::translate("StudentInfo", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteHonor->setText(QCoreApplication::translate("StudentInfo", "\345\210\240\351\231\244", nullptr));
        pushButton_exportHonor->setText(QCoreApplication::translate("StudentInfo", "\345\257\274\345\207\272Excel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("StudentInfo", "\350\216\267\345\245\226", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget_debt->horizontalHeaderItem(0);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("StudentInfo", "\347\247\221\347\233\256", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget_debt->horizontalHeaderItem(1);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("StudentInfo", "\345\210\206\346\225\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem12 = tableWidget_debt->horizontalHeaderItem(2);
        ___qtablewidgetitem12->setText(QCoreApplication::translate("StudentInfo", "\347\261\273\345\236\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem13 = tableWidget_debt->horizontalHeaderItem(3);
        ___qtablewidgetitem13->setText(QCoreApplication::translate("StudentInfo", "\346\227\245\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem14 = tableWidget_debt->horizontalHeaderItem(4);
        ___qtablewidgetitem14->setText(QCoreApplication::translate("StudentInfo", "\345\255\246\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem15 = tableWidget_debt->horizontalHeaderItem(5);
        ___qtablewidgetitem15->setText(QCoreApplication::translate("StudentInfo", "\345\271\264\344\273\275", nullptr));
        pushButton_addDebt->setText(QCoreApplication::translate("StudentInfo", "\346\267\273\345\212\240", nullptr));
        pushButton_editDebt->setText(QCoreApplication::translate("StudentInfo", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteDebt->setText(QCoreApplication::translate("StudentInfo", "\345\210\240\351\231\244", nullptr));
        pushButton_exportDebt->setText(QCoreApplication::translate("StudentInfo", "\345\257\274\345\207\272Excel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("StudentInfo", "\346\214\202\347\247\221", nullptr));
        QTableWidgetItem *___qtablewidgetitem16 = tableWidget_scholarShip->horizontalHeaderItem(0);
        ___qtablewidgetitem16->setText(QCoreApplication::translate("StudentInfo", "\345\245\226\345\255\246\351\207\221", nullptr));
        QTableWidgetItem *___qtablewidgetitem17 = tableWidget_scholarShip->horizontalHeaderItem(1);
        ___qtablewidgetitem17->setText(QCoreApplication::translate("StudentInfo", "\346\227\245\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem18 = tableWidget_scholarShip->horizontalHeaderItem(2);
        ___qtablewidgetitem18->setText(QCoreApplication::translate("StudentInfo", "\345\255\246\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem19 = tableWidget_scholarShip->horizontalHeaderItem(3);
        ___qtablewidgetitem19->setText(QCoreApplication::translate("StudentInfo", "\345\271\264\344\273\275", nullptr));
        pushButton_addScholarShip->setText(QCoreApplication::translate("StudentInfo", "\346\267\273\345\212\240", nullptr));
        pushButton_editScholarShip->setText(QCoreApplication::translate("StudentInfo", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteScholarShip->setText(QCoreApplication::translate("StudentInfo", "\345\210\240\351\231\244", nullptr));
        pushButton_exportScholarShip->setText(QCoreApplication::translate("StudentInfo", "\345\257\274\345\207\272Excel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QCoreApplication::translate("StudentInfo", "\345\245\226\345\255\246\351\207\221", nullptr));
        QTableWidgetItem *___qtablewidgetitem20 = tableWidget_punish->horizontalHeaderItem(0);
        ___qtablewidgetitem20->setText(QCoreApplication::translate("StudentInfo", "\345\216\237\345\233\240", nullptr));
        QTableWidgetItem *___qtablewidgetitem21 = tableWidget_punish->horizontalHeaderItem(1);
        ___qtablewidgetitem21->setText(QCoreApplication::translate("StudentInfo", "\345\244\204\347\220\206", nullptr));
        QTableWidgetItem *___qtablewidgetitem22 = tableWidget_punish->horizontalHeaderItem(2);
        ___qtablewidgetitem22->setText(QCoreApplication::translate("StudentInfo", "\346\227\245\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem23 = tableWidget_punish->horizontalHeaderItem(3);
        ___qtablewidgetitem23->setText(QCoreApplication::translate("StudentInfo", "\345\255\246\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem24 = tableWidget_punish->horizontalHeaderItem(4);
        ___qtablewidgetitem24->setText(QCoreApplication::translate("StudentInfo", "\345\271\264\344\273\275", nullptr));
        QTableWidgetItem *___qtablewidgetitem25 = tableWidget_punish->horizontalHeaderItem(5);
        ___qtablewidgetitem25->setText(QCoreApplication::translate("StudentInfo", "\347\273\223\346\236\234", nullptr));
        pushButton_addPunish->setText(QCoreApplication::translate("StudentInfo", "\346\267\273\345\212\240", nullptr));
        pushButton_editPunish->setText(QCoreApplication::translate("StudentInfo", "\347\274\226\350\276\221", nullptr));
        pushButton_deletePunish->setText(QCoreApplication::translate("StudentInfo", "\345\210\240\351\231\244", nullptr));
        pushButton_exportPunish->setText(QCoreApplication::translate("StudentInfo", "\345\257\274\345\207\272Excel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QCoreApplication::translate("StudentInfo", "\345\244\204\345\210\206", nullptr));
        label_19->setText(QCoreApplication::translate("StudentInfo", "\350\241\200\345\236\213", nullptr));
        label_20->setText(QCoreApplication::translate("StudentInfo", "\347\236\263\350\211\262", nullptr));
        label_21->setText(QCoreApplication::translate("StudentInfo", "\350\202\244\350\211\262", nullptr));
        groupBox->setTitle(QCoreApplication::translate("StudentInfo", "\345\205\266\344\273\226\344\277\241\346\201\257", nullptr));
        label_23->setText(QCoreApplication::translate("StudentInfo", "\347\210\266\344\272\262\345\247\223\345\220\215", nullptr));
        checkBox_father_workToTheState->setText(QCoreApplication::translate("StudentInfo", "\344\270\272\345\233\275\345\256\266\345\267\245\344\275\234", nullptr));
        checkBox_father_selfEmployed->setText(QCoreApplication::translate("StudentInfo", "\350\207\252\347\224\261\350\201\214\344\270\232\350\200\205", nullptr));
        label_24->setText(QCoreApplication::translate("StudentInfo", "\346\257\215\344\272\262\345\247\223\345\220\215", nullptr));
        checkBox_mother_workToTheState->setText(QCoreApplication::translate("StudentInfo", "\344\270\272\345\233\275\345\256\266\345\267\245\344\275\234", nullptr));
        checkBox_mother_selfEmployed->setText(QCoreApplication::translate("StudentInfo", "\350\207\252\347\224\261\350\201\214\344\270\232\350\200\205", nullptr));
        label_25->setText(QCoreApplication::translate("StudentInfo", "\347\210\266\346\257\215\347\232\204\345\205\266\344\273\226\344\277\241\346\201\257", nullptr));
        groupBox_file->setTitle(QCoreApplication::translate("StudentInfo", "\345\255\246\347\224\237\346\226\207\344\273\266\351\223\276\346\216\245", nullptr));
        pushButton_openFile1->setText(QString());
        pushButton_openFile3->setText(QString());
        pushButton_openFile4->setText(QString());
        pushButton_saveFile1->setText(QString());
        pushButton_openFile2->setText(QString());
        pushButton_saveFile2->setText(QString());
        pushButton_saveFile3->setText(QString());
        pushButton_saveFile4->setText(QString());
        pushButton_openPhoto->setText(QString());
        pushButton_cancelPhoto->setText(QString());
        label_27->setText(QCoreApplication::translate("StudentInfo", "\345\255\246\347\224\237\347\205\247\347\211\207", nullptr));
        label_28->setText(QCoreApplication::translate("StudentInfo", "\345\205\266\344\273\226\345\205\264\350\266\243", nullptr));
        pushButton_dataDictionary->setText(QCoreApplication::translate("StudentInfo", "\346\225\260\346\215\256\345\272\223", nullptr));
        pushButton_save->setText(QCoreApplication::translate("StudentInfo", "\344\277\235\345\255\230", nullptr));
        pushButton_exit->setText(QCoreApplication::translate("StudentInfo", "\345\205\263\351\227\255", nullptr));
    } // retranslateUi

};

namespace Ui {
    class StudentInfo: public Ui_StudentInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STUDENTINFO_H
