/********************************************************************************
** Form generated from reading UI file 'options.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPTIONS_H
#define UI_OPTIONS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Options
{
public:
    QAction *action;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout_2;
    QListWidget *listWidget;
    QVBoxLayout *verticalLayout_2;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QWidget *page_appearance;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_theme;
    QComboBox *comboBox_theme;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_return;
    QPushButton *pushButton_confirm;
    QPushButton *pushButton_apply;
    QSpacerItem *horizontalSpacer;
    QMenuBar *menubar;
    QMenu *menu_file;
    QMenu *menu_edit;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *Options)
    {
        if (Options->objectName().isEmpty())
            Options->setObjectName(QString::fromUtf8("Options"));
        Options->resize(1101, 603);
        action = new QAction(Options);
        action->setObjectName(QString::fromUtf8("action"));
        centralwidget = new QWidget(Options);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout_2 = new QHBoxLayout(centralwidget);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        listWidget = new QListWidget(centralwidget);
        new QListWidgetItem(listWidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(listWidget->sizePolicy().hasHeightForWidth());
        listWidget->setSizePolicy(sizePolicy);
        listWidget->setMinimumSize(QSize(100, 0));
        listWidget->setMaximumSize(QSize(100, 16777215));
        listWidget->setBatchSize(100);

        horizontalLayout_2->addWidget(listWidget);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        stackedWidget = new QStackedWidget(centralwidget);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        page->setEnabled(true);
        stackedWidget->addWidget(page);
        page_appearance = new QWidget();
        page_appearance->setObjectName(QString::fromUtf8("page_appearance"));
        page_appearance->setEnabled(true);
        verticalLayoutWidget = new QWidget(page_appearance);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 160, 80));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 17);
        label_theme = new QLabel(verticalLayoutWidget);
        label_theme->setObjectName(QString::fromUtf8("label_theme"));

        verticalLayout->addWidget(label_theme);

        comboBox_theme = new QComboBox(verticalLayoutWidget);
        comboBox_theme->setObjectName(QString::fromUtf8("comboBox_theme"));

        verticalLayout->addWidget(comboBox_theme);

        stackedWidget->addWidget(page_appearance);

        verticalLayout_2->addWidget(stackedWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButton_return = new QPushButton(centralwidget);
        pushButton_return->setObjectName(QString::fromUtf8("pushButton_return"));

        horizontalLayout->addWidget(pushButton_return);

        pushButton_confirm = new QPushButton(centralwidget);
        pushButton_confirm->setObjectName(QString::fromUtf8("pushButton_confirm"));

        horizontalLayout->addWidget(pushButton_confirm);

        pushButton_apply = new QPushButton(centralwidget);
        pushButton_apply->setObjectName(QString::fromUtf8("pushButton_apply"));

        horizontalLayout->addWidget(pushButton_apply);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout);


        horizontalLayout_2->addLayout(verticalLayout_2);

        Options->setCentralWidget(centralwidget);
        menubar = new QMenuBar(Options);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1101, 20));
        menu_file = new QMenu(menubar);
        menu_file->setObjectName(QString::fromUtf8("menu_file"));
        menu_edit = new QMenu(menubar);
        menu_edit->setObjectName(QString::fromUtf8("menu_edit"));
        Options->setMenuBar(menubar);
        statusbar = new QStatusBar(Options);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        Options->setStatusBar(statusbar);

        menubar->addAction(menu_file->menuAction());
        menubar->addAction(menu_edit->menuAction());
        menu_file->addAction(action);

        retranslateUi(Options);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(Options);
    } // setupUi

    void retranslateUi(QMainWindow *Options)
    {
        Options->setWindowTitle(QCoreApplication::translate("Options", "MainWindow", nullptr));
        action->setText(QCoreApplication::translate("Options", "\351\200\200\345\207\272", nullptr));

        const bool __sortingEnabled = listWidget->isSortingEnabled();
        listWidget->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = listWidget->item(0);
        ___qlistwidgetitem->setText(QCoreApplication::translate("Options", "\345\244\226\350\247\202", nullptr));
        listWidget->setSortingEnabled(__sortingEnabled);

        label_theme->setText(QCoreApplication::translate("Options", "\344\270\273\351\242\230", nullptr));
        pushButton_return->setText(QCoreApplication::translate("Options", "\350\277\224\345\233\236", nullptr));
        pushButton_confirm->setText(QCoreApplication::translate("Options", "\347\241\256\350\256\244", nullptr));
        pushButton_apply->setText(QCoreApplication::translate("Options", "\345\272\224\347\224\250", nullptr));
        menu_file->setTitle(QCoreApplication::translate("Options", "\346\226\207\344\273\266", nullptr));
        menu_edit->setTitle(QCoreApplication::translate("Options", "\347\274\226\350\276\221", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Options: public Ui_Options {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPTIONS_H
