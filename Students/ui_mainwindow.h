/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_aboutQt;
    QAction *action_options;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_4;
    QFrame *frame_main;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QLabel *label_welcome;
    QPushButton *pushButton_exit;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QFormLayout *formLayout_6;
    QLabel *label_3;
    QLineEdit *lineEdit_name;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_4;
    QCheckBox *checkBox_dataOfAdmission;
    QDateEdit *dateEdit_dataOfAdmission;
    QLabel *label_6;
    QComboBox *comboBox_province;
    QLabel *label_2;
    QLineEdit *lineEdit_number;
    QFormLayout *formLayout_2;
    QLabel *label_7;
    QComboBox *comboBox_profession;
    QComboBox *comboBox_city;
    QLabel *label_9;
    QLabel *label_8;
    QComboBox *comboBox_university;
    QComboBox *comboBox_status;
    QLabel *label_11;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton_search;
    QPushButton *pushButton_clean;
    QTableWidget *tableWidget_info;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton_add;
    QPushButton *pushButton_editOrView;
    QPushButton *pushButton_delete;
    QPushButton *pushButton_export;
    QPushButton *pushButton_import;
    QPushButton *pushButton_print;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton_dataDictionary;
    QPushButton *pushButton_user;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_7;
    QGridLayout *gridLayout;
    QLabel *label_14;
    QLabel *label;
    QLabel *label_12;
    QLineEdit *lineEdit_name_2;
    QLabel *label_10;
    QLabel *label_15;
    QComboBox *comboBox_blood;
    QLabel *label_13;
    QComboBox *comboBox_eye;
    QComboBox *comboBox_skin;
    QHBoxLayout *horizontalLayout_6;
    QCheckBox *checkBox_dataOfGraduation;
    QDateEdit *dateEdit_dataOfGraduation;
    QComboBox *comboBox_sex;
    QLabel *label_4;
    QLabel *label_16;
    QComboBox *comboBox_socialStatus;
    QHBoxLayout *horizontalLayout;
    QCheckBox *checkBox_dataOfAdmission_2;
    QDateEdit *dateEdit_dataOfAdmission_2;
    QPushButton *pushButton_search_2;
    QTableWidget *tableWidget_info_2;
    QWidget *tab_3;
    QMenuBar *menubar;
    QMenu *menu_tool;
    QMenu *menu_help;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1090, 820);
        MainWindow->setMinimumSize(QSize(1090, 820));
        MainWindow->setMaximumSize(QSize(1090, 820));
        action_aboutQt = new QAction(MainWindow);
        action_aboutQt->setObjectName(QString::fromUtf8("action_aboutQt"));
        action_options = new QAction(MainWindow);
        action_options->setObjectName(QString::fromUtf8("action_options"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_4 = new QVBoxLayout(centralwidget);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        frame_main = new QFrame(centralwidget);
        frame_main->setObjectName(QString::fromUtf8("frame_main"));
        frame_main->setMinimumSize(QSize(0, 150));
        frame_main->setStyleSheet(QString::fromUtf8("#frame_main{\n"
"Border-image:url(:/images/login_bg.png);\n"
"}"));
        frame_main->setFrameShape(QFrame::StyledPanel);
        frame_main->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame_main);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        label_welcome = new QLabel(frame_main);
        label_welcome->setObjectName(QString::fromUtf8("label_welcome"));

        horizontalLayout_3->addWidget(label_welcome);

        pushButton_exit = new QPushButton(frame_main);
        pushButton_exit->setObjectName(QString::fromUtf8("pushButton_exit"));
        pushButton_exit->setMinimumSize(QSize(40, 40));
        pushButton_exit->setMaximumSize(QSize(40, 40));
        pushButton_exit->setStyleSheet(QString::fromUtf8("#pushButton_exit{\n"
"Border-image:url(:/images/exit.png);\n"
"}\n"
"#pushButton_exit:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}\n"
"#pushButton_openPhoto:default{\n"
"\n"
"}\n"
"#pushButton_openPhoto:pressed{\n"
"\n"
"}"));

        horizontalLayout_3->addWidget(pushButton_exit);


        verticalLayout_4->addWidget(frame_main);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout = new QVBoxLayout(tab);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, -1, 0, -1);
        formLayout_6 = new QFormLayout();
        formLayout_6->setObjectName(QString::fromUtf8("formLayout_6"));
        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout_6->setWidget(0, QFormLayout::LabelRole, label_3);

        lineEdit_name = new QLineEdit(tab);
        lineEdit_name->setObjectName(QString::fromUtf8("lineEdit_name"));

        formLayout_6->setWidget(0, QFormLayout::FieldRole, lineEdit_name);

        label_5 = new QLabel(tab);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout_6->setWidget(1, QFormLayout::LabelRole, label_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        checkBox_dataOfAdmission = new QCheckBox(tab);
        checkBox_dataOfAdmission->setObjectName(QString::fromUtf8("checkBox_dataOfAdmission"));

        horizontalLayout_4->addWidget(checkBox_dataOfAdmission);

        dateEdit_dataOfAdmission = new QDateEdit(tab);
        dateEdit_dataOfAdmission->setObjectName(QString::fromUtf8("dateEdit_dataOfAdmission"));
        dateEdit_dataOfAdmission->setEnabled(true);

        horizontalLayout_4->addWidget(dateEdit_dataOfAdmission);

        label_6 = new QLabel(tab);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_4->addWidget(label_6);

        comboBox_province = new QComboBox(tab);
        comboBox_province->setObjectName(QString::fromUtf8("comboBox_province"));

        horizontalLayout_4->addWidget(comboBox_province);


        formLayout_6->setLayout(1, QFormLayout::FieldRole, horizontalLayout_4);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        formLayout_6->setWidget(2, QFormLayout::LabelRole, label_2);

        lineEdit_number = new QLineEdit(tab);
        lineEdit_number->setObjectName(QString::fromUtf8("lineEdit_number"));

        formLayout_6->setWidget(2, QFormLayout::FieldRole, lineEdit_number);


        horizontalLayout_5->addLayout(formLayout_6);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setHorizontalSpacing(6);
        formLayout_2->setVerticalSpacing(6);
        formLayout_2->setContentsMargins(-1, -1, -1, 0);
        label_7 = new QLabel(tab);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_7);

        comboBox_profession = new QComboBox(tab);
        comboBox_profession->setObjectName(QString::fromUtf8("comboBox_profession"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, comboBox_profession);

        comboBox_city = new QComboBox(tab);
        comboBox_city->setObjectName(QString::fromUtf8("comboBox_city"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, comboBox_city);

        label_9 = new QLabel(tab);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_9);

        label_8 = new QLabel(tab);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_8);

        comboBox_university = new QComboBox(tab);
        comboBox_university->setObjectName(QString::fromUtf8("comboBox_university"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, comboBox_university);

        comboBox_status = new QComboBox(tab);
        comboBox_status->setObjectName(QString::fromUtf8("comboBox_status"));

        formLayout_2->setWidget(3, QFormLayout::FieldRole, comboBox_status);

        label_11 = new QLabel(tab);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_11);


        horizontalLayout_5->addLayout(formLayout_2);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetDefaultConstraint);
        pushButton_search = new QPushButton(tab);
        pushButton_search->setObjectName(QString::fromUtf8("pushButton_search"));

        verticalLayout_2->addWidget(pushButton_search);

        pushButton_clean = new QPushButton(tab);
        pushButton_clean->setObjectName(QString::fromUtf8("pushButton_clean"));

        verticalLayout_2->addWidget(pushButton_clean);


        horizontalLayout_5->addLayout(verticalLayout_2);

        horizontalLayout_5->setStretch(0, 2);
        horizontalLayout_5->setStretch(1, 2);
        horizontalLayout_5->setStretch(2, 1);

        verticalLayout->addLayout(horizontalLayout_5);

        tableWidget_info = new QTableWidget(tab);
        tableWidget_info->setObjectName(QString::fromUtf8("tableWidget_info"));
        tableWidget_info->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout->addWidget(tableWidget_info);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pushButton_add = new QPushButton(tab);
        pushButton_add->setObjectName(QString::fromUtf8("pushButton_add"));

        horizontalLayout_2->addWidget(pushButton_add);

        pushButton_editOrView = new QPushButton(tab);
        pushButton_editOrView->setObjectName(QString::fromUtf8("pushButton_editOrView"));

        horizontalLayout_2->addWidget(pushButton_editOrView);

        pushButton_delete = new QPushButton(tab);
        pushButton_delete->setObjectName(QString::fromUtf8("pushButton_delete"));

        horizontalLayout_2->addWidget(pushButton_delete);

        pushButton_export = new QPushButton(tab);
        pushButton_export->setObjectName(QString::fromUtf8("pushButton_export"));

        horizontalLayout_2->addWidget(pushButton_export);

        pushButton_import = new QPushButton(tab);
        pushButton_import->setObjectName(QString::fromUtf8("pushButton_import"));

        horizontalLayout_2->addWidget(pushButton_import);

        pushButton_print = new QPushButton(tab);
        pushButton_print->setObjectName(QString::fromUtf8("pushButton_print"));

        horizontalLayout_2->addWidget(pushButton_print);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        pushButton_dataDictionary = new QPushButton(tab);
        pushButton_dataDictionary->setObjectName(QString::fromUtf8("pushButton_dataDictionary"));

        horizontalLayout_2->addWidget(pushButton_dataDictionary);

        pushButton_user = new QPushButton(tab);
        pushButton_user->setObjectName(QString::fromUtf8("pushButton_user"));

        horizontalLayout_2->addWidget(pushButton_user);


        verticalLayout->addLayout(horizontalLayout_2);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        verticalLayout_5 = new QVBoxLayout(tab_2);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_14 = new QLabel(tab_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout->addWidget(label_14, 0, 3, 1, 1);

        label = new QLabel(tab_2);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_12 = new QLabel(tab_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout->addWidget(label_12, 0, 4, 1, 1);

        lineEdit_name_2 = new QLineEdit(tab_2);
        lineEdit_name_2->setObjectName(QString::fromUtf8("lineEdit_name_2"));

        gridLayout->addWidget(lineEdit_name_2, 1, 0, 1, 1);

        label_10 = new QLabel(tab_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 0, 1, 1, 1);

        label_15 = new QLabel(tab_2);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout->addWidget(label_15, 0, 7, 1, 1);

        comboBox_blood = new QComboBox(tab_2);
        comboBox_blood->setObjectName(QString::fromUtf8("comboBox_blood"));

        gridLayout->addWidget(comboBox_blood, 1, 4, 1, 1);

        label_13 = new QLabel(tab_2);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout->addWidget(label_13, 0, 5, 1, 1);

        comboBox_eye = new QComboBox(tab_2);
        comboBox_eye->setObjectName(QString::fromUtf8("comboBox_eye"));

        gridLayout->addWidget(comboBox_eye, 1, 3, 1, 1);

        comboBox_skin = new QComboBox(tab_2);
        comboBox_skin->setObjectName(QString::fromUtf8("comboBox_skin"));

        gridLayout->addWidget(comboBox_skin, 1, 2, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        checkBox_dataOfGraduation = new QCheckBox(tab_2);
        checkBox_dataOfGraduation->setObjectName(QString::fromUtf8("checkBox_dataOfGraduation"));

        horizontalLayout_6->addWidget(checkBox_dataOfGraduation);

        dateEdit_dataOfGraduation = new QDateEdit(tab_2);
        dateEdit_dataOfGraduation->setObjectName(QString::fromUtf8("dateEdit_dataOfGraduation"));

        horizontalLayout_6->addWidget(dateEdit_dataOfGraduation);


        gridLayout->addLayout(horizontalLayout_6, 1, 8, 1, 1);

        comboBox_sex = new QComboBox(tab_2);
        comboBox_sex->addItem(QString());
        comboBox_sex->addItem(QString());
        comboBox_sex->setObjectName(QString::fromUtf8("comboBox_sex"));

        gridLayout->addWidget(comboBox_sex, 1, 1, 1, 1);

        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 0, 2, 1, 1);

        label_16 = new QLabel(tab_2);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout->addWidget(label_16, 0, 8, 1, 1);

        comboBox_socialStatus = new QComboBox(tab_2);
        comboBox_socialStatus->setObjectName(QString::fromUtf8("comboBox_socialStatus"));

        gridLayout->addWidget(comboBox_socialStatus, 1, 5, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        checkBox_dataOfAdmission_2 = new QCheckBox(tab_2);
        checkBox_dataOfAdmission_2->setObjectName(QString::fromUtf8("checkBox_dataOfAdmission_2"));

        horizontalLayout->addWidget(checkBox_dataOfAdmission_2);

        dateEdit_dataOfAdmission_2 = new QDateEdit(tab_2);
        dateEdit_dataOfAdmission_2->setObjectName(QString::fromUtf8("dateEdit_dataOfAdmission_2"));

        horizontalLayout->addWidget(dateEdit_dataOfAdmission_2);


        gridLayout->addLayout(horizontalLayout, 1, 7, 1, 1);


        horizontalLayout_7->addLayout(gridLayout);

        pushButton_search_2 = new QPushButton(tab_2);
        pushButton_search_2->setObjectName(QString::fromUtf8("pushButton_search_2"));

        horizontalLayout_7->addWidget(pushButton_search_2);


        verticalLayout_5->addLayout(horizontalLayout_7);

        tableWidget_info_2 = new QTableWidget(tab_2);
        tableWidget_info_2->setObjectName(QString::fromUtf8("tableWidget_info_2"));
        tableWidget_info_2->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout_5->addWidget(tableWidget_info_2);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        tabWidget->addTab(tab_3, QString());

        verticalLayout_4->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1090, 20));
        menu_tool = new QMenu(menubar);
        menu_tool->setObjectName(QString::fromUtf8("menu_tool"));
        menu_help = new QMenu(menubar);
        menu_help->setObjectName(QString::fromUtf8("menu_help"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menu_tool->menuAction());
        menubar->addAction(menu_help->menuAction());
        menu_tool->addAction(action_options);
        menu_help->addAction(action_aboutQt);
        menu_help->addSeparator();

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        action_aboutQt->setText(QCoreApplication::translate("MainWindow", "\345\205\263\344\272\216Qt", nullptr));
        action_options->setText(QCoreApplication::translate("MainWindow", "\351\200\211\351\241\271", nullptr));
        label_welcome->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        pushButton_exit->setText(QString());
        label_3->setText(QCoreApplication::translate("MainWindow", "\345\247\223\345\220\215", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "\345\205\245\345\255\246\346\227\245\346\234\237", nullptr));
        checkBox_dataOfAdmission->setText(QString());
        label_6->setText(QCoreApplication::translate("MainWindow", "\347\234\201\344\273\275", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "\345\255\246\345\217\267", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "\344\270\223\344\270\232", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "\345\237\216\345\270\202", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "\345\244\247\345\255\246", nullptr));
        label_11->setText(QCoreApplication::translate("MainWindow", "\347\212\266\346\200\201", nullptr));
        pushButton_search->setText(QCoreApplication::translate("MainWindow", "\346\237\245\346\211\276", nullptr));
        pushButton_clean->setText(QCoreApplication::translate("MainWindow", "\346\270\205\351\231\244", nullptr));
        pushButton_add->setText(QCoreApplication::translate("MainWindow", "\346\267\273\345\212\240", nullptr));
        pushButton_editOrView->setText(QCoreApplication::translate("MainWindow", "\346\237\245\347\234\213/\347\274\226\350\276\221", nullptr));
        pushButton_delete->setText(QCoreApplication::translate("MainWindow", "\345\210\240\351\231\244", nullptr));
        pushButton_export->setText(QCoreApplication::translate("MainWindow", "\345\257\274\345\207\272Excel", nullptr));
        pushButton_import->setText(QCoreApplication::translate("MainWindow", "\345\257\274\345\205\245Excel", nullptr));
        pushButton_print->setText(QCoreApplication::translate("MainWindow", "\346\211\223\345\215\260", nullptr));
        pushButton_dataDictionary->setText(QCoreApplication::translate("MainWindow", "\346\225\260\346\215\256\345\272\223", nullptr));
        pushButton_user->setText(QCoreApplication::translate("MainWindow", "\347\224\250\346\210\267", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("MainWindow", "\346\237\245\346\211\276", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "\347\236\263\350\211\262", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "\345\247\223\345\220\215", nullptr));
        label_12->setText(QCoreApplication::translate("MainWindow", "\350\241\200\345\236\213", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "\346\200\247\345\210\253", nullptr));
        label_15->setText(QCoreApplication::translate("MainWindow", "\345\205\245\345\255\246\346\227\245\346\234\237", nullptr));
        label_13->setText(QCoreApplication::translate("MainWindow", "\347\244\276\344\272\244\351\200\224\345\276\204", nullptr));
        checkBox_dataOfGraduation->setText(QString());
        comboBox_sex->setItemText(0, QCoreApplication::translate("MainWindow", "\347\224\267", nullptr));
        comboBox_sex->setItemText(1, QCoreApplication::translate("MainWindow", "\345\245\263", nullptr));

        label_4->setText(QCoreApplication::translate("MainWindow", "\350\202\244\350\211\262", nullptr));
        label_16->setText(QCoreApplication::translate("MainWindow", "\346\257\225\344\270\232\346\227\245\346\234\237", nullptr));
        checkBox_dataOfAdmission_2->setText(QString());
        pushButton_search_2->setText(QCoreApplication::translate("MainWindow", "\346\237\245\346\211\276", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("MainWindow", "\351\253\230\347\272\247\346\237\245\346\211\276", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("MainWindow", "\345\210\206\345\270\203\345\233\276", nullptr));
        menu_tool->setTitle(QCoreApplication::translate("MainWindow", "\345\267\245\345\205\267", nullptr));
        menu_help->setTitle(QCoreApplication::translate("MainWindow", "\345\270\256\345\212\251", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
