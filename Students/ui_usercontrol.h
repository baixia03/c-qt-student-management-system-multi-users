/********************************************************************************
** Form generated from reading UI file 'usercontrol.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USERCONTROL_H
#define UI_USERCONTROL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_UserControl
{
public:
    QVBoxLayout *verticalLayout;
    QTableWidget *tableWidget;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *pushButton_add;
    QPushButton *pushButton_edit;
    QPushButton *pushButton_delete;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton_registry;

    void setupUi(QDialog *UserControl)
    {
        if (UserControl->objectName().isEmpty())
            UserControl->setObjectName(QString::fromUtf8("UserControl"));
        UserControl->resize(1050, 600);
        UserControl->setMinimumSize(QSize(1050, 600));
        UserControl->setMaximumSize(QSize(1050, 600));
        verticalLayout = new QVBoxLayout(UserControl);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        tableWidget = new QTableWidget(UserControl);
        if (tableWidget->columnCount() < 8)
            tableWidget->setColumnCount(8);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(7, __qtablewidgetitem7);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setMinimumSize(QSize(0, 0));
        tableWidget->setMaximumSize(QSize(16777215, 16777215));
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

        verticalLayout->addWidget(tableWidget);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        pushButton_add = new QPushButton(UserControl);
        pushButton_add->setObjectName(QString::fromUtf8("pushButton_add"));

        horizontalLayout_5->addWidget(pushButton_add);

        pushButton_edit = new QPushButton(UserControl);
        pushButton_edit->setObjectName(QString::fromUtf8("pushButton_edit"));

        horizontalLayout_5->addWidget(pushButton_edit);

        pushButton_delete = new QPushButton(UserControl);
        pushButton_delete->setObjectName(QString::fromUtf8("pushButton_delete"));

        horizontalLayout_5->addWidget(pushButton_delete);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        pushButton_registry = new QPushButton(UserControl);
        pushButton_registry->setObjectName(QString::fromUtf8("pushButton_registry"));

        horizontalLayout_5->addWidget(pushButton_registry);


        verticalLayout->addLayout(horizontalLayout_5);


        retranslateUi(UserControl);

        QMetaObject::connectSlotsByName(UserControl);
    } // setupUi

    void retranslateUi(QDialog *UserControl)
    {
        UserControl->setWindowTitle(QCoreApplication::translate("UserControl", "Dialog", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("UserControl", "\347\224\250\346\210\267", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("UserControl", "\350\257\273", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("UserControl", "\345\206\231", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("UserControl", "\345\210\240\351\231\244", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("UserControl", "\346\237\245\346\211\276", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("UserControl", "\346\211\223\345\215\260", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("UserControl", "\345\257\274\345\205\245", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->horizontalHeaderItem(7);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("UserControl", "\347\256\241\347\220\206\345\221\230", nullptr));
        pushButton_add->setText(QCoreApplication::translate("UserControl", "\346\267\273\345\212\240", nullptr));
        pushButton_edit->setText(QCoreApplication::translate("UserControl", "\347\274\226\350\276\221", nullptr));
        pushButton_delete->setText(QCoreApplication::translate("UserControl", "\345\210\240\351\231\244", nullptr));
        pushButton_registry->setText(QCoreApplication::translate("UserControl", "\350\256\260\345\275\225", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UserControl: public Ui_UserControl {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USERCONTROL_H
