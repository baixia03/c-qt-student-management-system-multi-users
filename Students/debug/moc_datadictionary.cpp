/****************************************************************************
** Meta object code from reading C++ file 'datadictionary.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../datadictionary.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'datadictionary.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DataDictionary_t {
    QByteArrayData data[51];
    char stringdata0[1394];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DataDictionary_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DataDictionary_t qt_meta_stringdata_DataDictionary = {
    {
QT_MOC_LITERAL(0, 0, 14), // "DataDictionary"
QT_MOC_LITERAL(1, 15, 14), // "needUpdataData"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 25), // "clickPushButton_addNation"
QT_MOC_LITERAL(4, 57, 26), // "clickPushButton_editNation"
QT_MOC_LITERAL(5, 84, 28), // "clickPushButton_deleteNation"
QT_MOC_LITERAL(6, 113, 28), // "clickPushButton_addPolitical"
QT_MOC_LITERAL(7, 142, 29), // "clickPushButton_editPolitical"
QT_MOC_LITERAL(8, 172, 31), // "clickPushButton_deletePolitical"
QT_MOC_LITERAL(9, 204, 27), // "clickPushButton_addProvince"
QT_MOC_LITERAL(10, 232, 28), // "clickPushButton_editProvince"
QT_MOC_LITERAL(11, 261, 30), // "clickPushButton_deleteProvince"
QT_MOC_LITERAL(12, 292, 23), // "clickPushButton_addCity"
QT_MOC_LITERAL(13, 316, 24), // "clickPushButton_editCity"
QT_MOC_LITERAL(14, 341, 26), // "clickPushButton_deleteCity"
QT_MOC_LITERAL(15, 368, 29), // "clickPushButton_addUniversity"
QT_MOC_LITERAL(16, 398, 30), // "clickPushButton_editUniversity"
QT_MOC_LITERAL(17, 429, 32), // "clickPushButton_deleteUniversity"
QT_MOC_LITERAL(18, 462, 26), // "clickPushButton_addCollege"
QT_MOC_LITERAL(19, 489, 27), // "clickPushButton_editCollege"
QT_MOC_LITERAL(20, 517, 29), // "clickPushButton_deleteCollege"
QT_MOC_LITERAL(21, 547, 29), // "clickPushButton_addProfession"
QT_MOC_LITERAL(22, 577, 30), // "clickPushButton_editProfession"
QT_MOC_LITERAL(23, 608, 32), // "clickPushButton_deleteProfession"
QT_MOC_LITERAL(24, 641, 25), // "clickPushButton_addStatus"
QT_MOC_LITERAL(25, 667, 26), // "clickPushButton_editStatus"
QT_MOC_LITERAL(26, 694, 28), // "clickPushButton_deleteStatus"
QT_MOC_LITERAL(27, 723, 31), // "clickPushButton_addSocialStatus"
QT_MOC_LITERAL(28, 755, 32), // "clickPushButton_editSocialStatus"
QT_MOC_LITERAL(29, 788, 34), // "clickPushButton_deleteSocialS..."
QT_MOC_LITERAL(30, 823, 24), // "clickPushButton_addBlood"
QT_MOC_LITERAL(31, 848, 25), // "clickPushButton_editBlood"
QT_MOC_LITERAL(32, 874, 27), // "clickPushButton_deleteBlood"
QT_MOC_LITERAL(33, 902, 22), // "clickPushButton_addEye"
QT_MOC_LITERAL(34, 925, 23), // "clickPushButton_editEye"
QT_MOC_LITERAL(35, 949, 25), // "clickPushButton_deleteEye"
QT_MOC_LITERAL(36, 975, 23), // "clickPushButton_addSkin"
QT_MOC_LITERAL(37, 999, 24), // "clickPushButton_editSkin"
QT_MOC_LITERAL(38, 1024, 26), // "clickPushButton_deleteSkin"
QT_MOC_LITERAL(39, 1051, 26), // "clickPushButton_addSubject"
QT_MOC_LITERAL(40, 1078, 27), // "clickPushButton_editSubject"
QT_MOC_LITERAL(41, 1106, 29), // "clickPushButton_deleteSubject"
QT_MOC_LITERAL(42, 1136, 23), // "clickPushButton_addYear"
QT_MOC_LITERAL(43, 1160, 24), // "clickPushButton_editYear"
QT_MOC_LITERAL(44, 1185, 26), // "clickPushButton_deleteYear"
QT_MOC_LITERAL(45, 1212, 27), // "clickPushButton_addSemester"
QT_MOC_LITERAL(46, 1240, 28), // "clickPushButton_editSemester"
QT_MOC_LITERAL(47, 1269, 30), // "clickPushButton_deleteSemester"
QT_MOC_LITERAL(48, 1300, 29), // "clickPushButton_addTypeOfExam"
QT_MOC_LITERAL(49, 1330, 30), // "clickPushButton_editTypeOfExam"
QT_MOC_LITERAL(50, 1361, 32) // "clickPushButton_deleteTypeOfExam"

    },
    "DataDictionary\0needUpdataData\0\0"
    "clickPushButton_addNation\0"
    "clickPushButton_editNation\0"
    "clickPushButton_deleteNation\0"
    "clickPushButton_addPolitical\0"
    "clickPushButton_editPolitical\0"
    "clickPushButton_deletePolitical\0"
    "clickPushButton_addProvince\0"
    "clickPushButton_editProvince\0"
    "clickPushButton_deleteProvince\0"
    "clickPushButton_addCity\0"
    "clickPushButton_editCity\0"
    "clickPushButton_deleteCity\0"
    "clickPushButton_addUniversity\0"
    "clickPushButton_editUniversity\0"
    "clickPushButton_deleteUniversity\0"
    "clickPushButton_addCollege\0"
    "clickPushButton_editCollege\0"
    "clickPushButton_deleteCollege\0"
    "clickPushButton_addProfession\0"
    "clickPushButton_editProfession\0"
    "clickPushButton_deleteProfession\0"
    "clickPushButton_addStatus\0"
    "clickPushButton_editStatus\0"
    "clickPushButton_deleteStatus\0"
    "clickPushButton_addSocialStatus\0"
    "clickPushButton_editSocialStatus\0"
    "clickPushButton_deleteSocialStatus\0"
    "clickPushButton_addBlood\0"
    "clickPushButton_editBlood\0"
    "clickPushButton_deleteBlood\0"
    "clickPushButton_addEye\0clickPushButton_editEye\0"
    "clickPushButton_deleteEye\0"
    "clickPushButton_addSkin\0"
    "clickPushButton_editSkin\0"
    "clickPushButton_deleteSkin\0"
    "clickPushButton_addSubject\0"
    "clickPushButton_editSubject\0"
    "clickPushButton_deleteSubject\0"
    "clickPushButton_addYear\0"
    "clickPushButton_editYear\0"
    "clickPushButton_deleteYear\0"
    "clickPushButton_addSemester\0"
    "clickPushButton_editSemester\0"
    "clickPushButton_deleteSemester\0"
    "clickPushButton_addTypeOfExam\0"
    "clickPushButton_editTypeOfExam\0"
    "clickPushButton_deleteTypeOfExam"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DataDictionary[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      49,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  259,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,  260,    2, 0x08 /* Private */,
       4,    0,  261,    2, 0x08 /* Private */,
       5,    0,  262,    2, 0x08 /* Private */,
       6,    0,  263,    2, 0x08 /* Private */,
       7,    0,  264,    2, 0x08 /* Private */,
       8,    0,  265,    2, 0x08 /* Private */,
       9,    0,  266,    2, 0x08 /* Private */,
      10,    0,  267,    2, 0x08 /* Private */,
      11,    0,  268,    2, 0x08 /* Private */,
      12,    0,  269,    2, 0x08 /* Private */,
      13,    0,  270,    2, 0x08 /* Private */,
      14,    0,  271,    2, 0x08 /* Private */,
      15,    0,  272,    2, 0x08 /* Private */,
      16,    0,  273,    2, 0x08 /* Private */,
      17,    0,  274,    2, 0x08 /* Private */,
      18,    0,  275,    2, 0x08 /* Private */,
      19,    0,  276,    2, 0x08 /* Private */,
      20,    0,  277,    2, 0x08 /* Private */,
      21,    0,  278,    2, 0x08 /* Private */,
      22,    0,  279,    2, 0x08 /* Private */,
      23,    0,  280,    2, 0x08 /* Private */,
      24,    0,  281,    2, 0x08 /* Private */,
      25,    0,  282,    2, 0x08 /* Private */,
      26,    0,  283,    2, 0x08 /* Private */,
      27,    0,  284,    2, 0x08 /* Private */,
      28,    0,  285,    2, 0x08 /* Private */,
      29,    0,  286,    2, 0x08 /* Private */,
      30,    0,  287,    2, 0x08 /* Private */,
      31,    0,  288,    2, 0x08 /* Private */,
      32,    0,  289,    2, 0x08 /* Private */,
      33,    0,  290,    2, 0x08 /* Private */,
      34,    0,  291,    2, 0x08 /* Private */,
      35,    0,  292,    2, 0x08 /* Private */,
      36,    0,  293,    2, 0x08 /* Private */,
      37,    0,  294,    2, 0x08 /* Private */,
      38,    0,  295,    2, 0x08 /* Private */,
      39,    0,  296,    2, 0x08 /* Private */,
      40,    0,  297,    2, 0x08 /* Private */,
      41,    0,  298,    2, 0x08 /* Private */,
      42,    0,  299,    2, 0x08 /* Private */,
      43,    0,  300,    2, 0x08 /* Private */,
      44,    0,  301,    2, 0x08 /* Private */,
      45,    0,  302,    2, 0x08 /* Private */,
      46,    0,  303,    2, 0x08 /* Private */,
      47,    0,  304,    2, 0x08 /* Private */,
      48,    0,  305,    2, 0x08 /* Private */,
      49,    0,  306,    2, 0x08 /* Private */,
      50,    0,  307,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void DataDictionary::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DataDictionary *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->needUpdataData(); break;
        case 1: _t->clickPushButton_addNation(); break;
        case 2: _t->clickPushButton_editNation(); break;
        case 3: _t->clickPushButton_deleteNation(); break;
        case 4: _t->clickPushButton_addPolitical(); break;
        case 5: _t->clickPushButton_editPolitical(); break;
        case 6: _t->clickPushButton_deletePolitical(); break;
        case 7: _t->clickPushButton_addProvince(); break;
        case 8: _t->clickPushButton_editProvince(); break;
        case 9: _t->clickPushButton_deleteProvince(); break;
        case 10: _t->clickPushButton_addCity(); break;
        case 11: _t->clickPushButton_editCity(); break;
        case 12: _t->clickPushButton_deleteCity(); break;
        case 13: _t->clickPushButton_addUniversity(); break;
        case 14: _t->clickPushButton_editUniversity(); break;
        case 15: _t->clickPushButton_deleteUniversity(); break;
        case 16: _t->clickPushButton_addCollege(); break;
        case 17: _t->clickPushButton_editCollege(); break;
        case 18: _t->clickPushButton_deleteCollege(); break;
        case 19: _t->clickPushButton_addProfession(); break;
        case 20: _t->clickPushButton_editProfession(); break;
        case 21: _t->clickPushButton_deleteProfession(); break;
        case 22: _t->clickPushButton_addStatus(); break;
        case 23: _t->clickPushButton_editStatus(); break;
        case 24: _t->clickPushButton_deleteStatus(); break;
        case 25: _t->clickPushButton_addSocialStatus(); break;
        case 26: _t->clickPushButton_editSocialStatus(); break;
        case 27: _t->clickPushButton_deleteSocialStatus(); break;
        case 28: _t->clickPushButton_addBlood(); break;
        case 29: _t->clickPushButton_editBlood(); break;
        case 30: _t->clickPushButton_deleteBlood(); break;
        case 31: _t->clickPushButton_addEye(); break;
        case 32: _t->clickPushButton_editEye(); break;
        case 33: _t->clickPushButton_deleteEye(); break;
        case 34: _t->clickPushButton_addSkin(); break;
        case 35: _t->clickPushButton_editSkin(); break;
        case 36: _t->clickPushButton_deleteSkin(); break;
        case 37: _t->clickPushButton_addSubject(); break;
        case 38: _t->clickPushButton_editSubject(); break;
        case 39: _t->clickPushButton_deleteSubject(); break;
        case 40: _t->clickPushButton_addYear(); break;
        case 41: _t->clickPushButton_editYear(); break;
        case 42: _t->clickPushButton_deleteYear(); break;
        case 43: _t->clickPushButton_addSemester(); break;
        case 44: _t->clickPushButton_editSemester(); break;
        case 45: _t->clickPushButton_deleteSemester(); break;
        case 46: _t->clickPushButton_addTypeOfExam(); break;
        case 47: _t->clickPushButton_editTypeOfExam(); break;
        case 48: _t->clickPushButton_deleteTypeOfExam(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (DataDictionary::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DataDictionary::needUpdataData)) {
                *result = 0;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject DataDictionary::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_DataDictionary.data,
    qt_meta_data_DataDictionary,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DataDictionary::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DataDictionary::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DataDictionary.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int DataDictionary::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 49)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 49;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 49)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 49;
    }
    return _id;
}

// SIGNAL 0
void DataDictionary::needUpdataData()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
