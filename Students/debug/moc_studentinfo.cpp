/****************************************************************************
** Meta object code from reading C++ file 'studentinfo.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../studentinfo.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'studentinfo.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_StudentInfo_t {
    QByteArrayData data[30];
    char stringdata0[755];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_StudentInfo_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_StudentInfo_t qt_meta_stringdata_StudentInfo = {
    {
QT_MOC_LITERAL(0, 0, 11), // "StudentInfo"
QT_MOC_LITERAL(1, 12, 14), // "needUpdataData"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 20), // "clickPushButton_save"
QT_MOC_LITERAL(4, 49, 30), // "clickPushButton_dataDictionary"
QT_MOC_LITERAL(5, 80, 24), // "clickPushButton_addGrade"
QT_MOC_LITERAL(6, 105, 25), // "clickPushButton_editGrade"
QT_MOC_LITERAL(7, 131, 27), // "clickPushButton_deleteGrade"
QT_MOC_LITERAL(8, 159, 24), // "clickPushButton_addHonor"
QT_MOC_LITERAL(9, 184, 25), // "clickPushButton_editHonor"
QT_MOC_LITERAL(10, 210, 27), // "clickPushButton_deleteHonor"
QT_MOC_LITERAL(11, 238, 23), // "clickPushButton_addDebt"
QT_MOC_LITERAL(12, 262, 24), // "clickPushButton_editDebt"
QT_MOC_LITERAL(13, 287, 26), // "clickPushButton_deleteDebt"
QT_MOC_LITERAL(14, 314, 30), // "clickPushButton_addScholarShip"
QT_MOC_LITERAL(15, 345, 31), // "clickPushButton_editScholarShip"
QT_MOC_LITERAL(16, 377, 33), // "clickPushButton_deleteScholar..."
QT_MOC_LITERAL(17, 411, 25), // "clickPushButton_addPunish"
QT_MOC_LITERAL(18, 437, 26), // "clickPushButton_editPunish"
QT_MOC_LITERAL(19, 464, 28), // "clickPushButton_deletePunish"
QT_MOC_LITERAL(20, 493, 25), // "clickPushButton_openFile1"
QT_MOC_LITERAL(21, 519, 25), // "clickPushButton_openFile2"
QT_MOC_LITERAL(22, 545, 25), // "clickPushButton_openFile3"
QT_MOC_LITERAL(23, 571, 25), // "clickPushButton_openFile4"
QT_MOC_LITERAL(24, 597, 25), // "clickPushButton_saveFile1"
QT_MOC_LITERAL(25, 623, 25), // "clickPushButton_saveFile2"
QT_MOC_LITERAL(26, 649, 25), // "clickPushButton_saveFile3"
QT_MOC_LITERAL(27, 675, 25), // "clickPushButton_saveFile4"
QT_MOC_LITERAL(28, 701, 25), // "clickPushButton_openPhoto"
QT_MOC_LITERAL(29, 727, 27) // "clickPushButton_cancelPhoto"

    },
    "StudentInfo\0needUpdataData\0\0"
    "clickPushButton_save\0"
    "clickPushButton_dataDictionary\0"
    "clickPushButton_addGrade\0"
    "clickPushButton_editGrade\0"
    "clickPushButton_deleteGrade\0"
    "clickPushButton_addHonor\0"
    "clickPushButton_editHonor\0"
    "clickPushButton_deleteHonor\0"
    "clickPushButton_addDebt\0"
    "clickPushButton_editDebt\0"
    "clickPushButton_deleteDebt\0"
    "clickPushButton_addScholarShip\0"
    "clickPushButton_editScholarShip\0"
    "clickPushButton_deleteScholarShip\0"
    "clickPushButton_addPunish\0"
    "clickPushButton_editPunish\0"
    "clickPushButton_deletePunish\0"
    "clickPushButton_openFile1\0"
    "clickPushButton_openFile2\0"
    "clickPushButton_openFile3\0"
    "clickPushButton_openFile4\0"
    "clickPushButton_saveFile1\0"
    "clickPushButton_saveFile2\0"
    "clickPushButton_saveFile3\0"
    "clickPushButton_saveFile4\0"
    "clickPushButton_openPhoto\0"
    "clickPushButton_cancelPhoto"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_StudentInfo[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      28,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  154,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,  155,    2, 0x08 /* Private */,
       4,    0,  156,    2, 0x08 /* Private */,
       5,    0,  157,    2, 0x08 /* Private */,
       6,    0,  158,    2, 0x08 /* Private */,
       7,    0,  159,    2, 0x08 /* Private */,
       8,    0,  160,    2, 0x08 /* Private */,
       9,    0,  161,    2, 0x08 /* Private */,
      10,    0,  162,    2, 0x08 /* Private */,
      11,    0,  163,    2, 0x08 /* Private */,
      12,    0,  164,    2, 0x08 /* Private */,
      13,    0,  165,    2, 0x08 /* Private */,
      14,    0,  166,    2, 0x08 /* Private */,
      15,    0,  167,    2, 0x08 /* Private */,
      16,    0,  168,    2, 0x08 /* Private */,
      17,    0,  169,    2, 0x08 /* Private */,
      18,    0,  170,    2, 0x08 /* Private */,
      19,    0,  171,    2, 0x08 /* Private */,
      20,    0,  172,    2, 0x08 /* Private */,
      21,    0,  173,    2, 0x08 /* Private */,
      22,    0,  174,    2, 0x08 /* Private */,
      23,    0,  175,    2, 0x08 /* Private */,
      24,    0,  176,    2, 0x08 /* Private */,
      25,    0,  177,    2, 0x08 /* Private */,
      26,    0,  178,    2, 0x08 /* Private */,
      27,    0,  179,    2, 0x08 /* Private */,
      28,    0,  180,    2, 0x08 /* Private */,
      29,    0,  181,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void StudentInfo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<StudentInfo *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->needUpdataData(); break;
        case 1: _t->clickPushButton_save(); break;
        case 2: _t->clickPushButton_dataDictionary(); break;
        case 3: _t->clickPushButton_addGrade(); break;
        case 4: _t->clickPushButton_editGrade(); break;
        case 5: _t->clickPushButton_deleteGrade(); break;
        case 6: _t->clickPushButton_addHonor(); break;
        case 7: _t->clickPushButton_editHonor(); break;
        case 8: _t->clickPushButton_deleteHonor(); break;
        case 9: _t->clickPushButton_addDebt(); break;
        case 10: _t->clickPushButton_editDebt(); break;
        case 11: _t->clickPushButton_deleteDebt(); break;
        case 12: _t->clickPushButton_addScholarShip(); break;
        case 13: _t->clickPushButton_editScholarShip(); break;
        case 14: _t->clickPushButton_deleteScholarShip(); break;
        case 15: _t->clickPushButton_addPunish(); break;
        case 16: _t->clickPushButton_editPunish(); break;
        case 17: _t->clickPushButton_deletePunish(); break;
        case 18: _t->clickPushButton_openFile1(); break;
        case 19: _t->clickPushButton_openFile2(); break;
        case 20: _t->clickPushButton_openFile3(); break;
        case 21: _t->clickPushButton_openFile4(); break;
        case 22: _t->clickPushButton_saveFile1(); break;
        case 23: _t->clickPushButton_saveFile2(); break;
        case 24: _t->clickPushButton_saveFile3(); break;
        case 25: _t->clickPushButton_saveFile4(); break;
        case 26: _t->clickPushButton_openPhoto(); break;
        case 27: _t->clickPushButton_cancelPhoto(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (StudentInfo::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&StudentInfo::needUpdataData)) {
                *result = 0;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject StudentInfo::staticMetaObject = { {
    QMetaObject::SuperData::link<QDialog::staticMetaObject>(),
    qt_meta_stringdata_StudentInfo.data,
    qt_meta_data_StudentInfo,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *StudentInfo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *StudentInfo::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_StudentInfo.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int StudentInfo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 28)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 28;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 28)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 28;
    }
    return _id;
}

// SIGNAL 0
void StudentInfo::needUpdataData()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
