/********************************************************************************
** Form generated from reading UI file 'scholarship.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCHOLARSHIP_H
#define UI_SCHOLARSHIP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ScholarShip
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_3;
    QLabel *label_4;
    QComboBox *comboBox_semester;
    QLabel *label_2;
    QLineEdit *lineEdit_count;
    QPushButton *pushButton_add1;
    QLabel *label_5;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QCheckBox *checkBox;
    QDateEdit *dateEdit;
    QComboBox *comboBox_year;
    QPushButton *pushButton_add2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;

    void setupUi(QDialog *ScholarShip)
    {
        if (ScholarShip->objectName().isEmpty())
            ScholarShip->setObjectName(QString::fromUtf8("ScholarShip"));
        ScholarShip->resize(299, 242);
        verticalLayout = new QVBoxLayout(ScholarShip);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(-1, -1, 0, -1);
        label_3 = new QLabel(ScholarShip);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 1, 0, 1, 1, Qt::AlignHCenter);

        label_4 = new QLabel(ScholarShip);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 2, 0, 1, 1, Qt::AlignHCenter);

        comboBox_semester = new QComboBox(ScholarShip);
        comboBox_semester->setObjectName(QString::fromUtf8("comboBox_semester"));

        gridLayout->addWidget(comboBox_semester, 2, 1, 1, 1);

        label_2 = new QLabel(ScholarShip);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1, Qt::AlignHCenter);

        lineEdit_count = new QLineEdit(ScholarShip);
        lineEdit_count->setObjectName(QString::fromUtf8("lineEdit_count"));

        gridLayout->addWidget(lineEdit_count, 0, 1, 1, 1);

        pushButton_add1 = new QPushButton(ScholarShip);
        pushButton_add1->setObjectName(QString::fromUtf8("pushButton_add1"));
        pushButton_add1->setMinimumSize(QSize(25, 25));
        pushButton_add1->setMaximumSize(QSize(25, 25));
        pushButton_add1->setStyleSheet(QString::fromUtf8("#pushButton_add1{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add1:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add1, 2, 2, 1, 1);

        label_5 = new QLabel(ScholarShip);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 3, 0, 1, 1, Qt::AlignHCenter);

        widget = new QWidget(ScholarShip);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMaximumSize(QSize(16777215, 60));
        widget->setLayoutDirection(Qt::LeftToRight);
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(3);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        checkBox = new QCheckBox(widget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        horizontalLayout->addWidget(checkBox);

        dateEdit = new QDateEdit(widget);
        dateEdit->setObjectName(QString::fromUtf8("dateEdit"));

        horizontalLayout->addWidget(dateEdit);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 7);

        gridLayout->addWidget(widget, 1, 1, 1, 1);

        comboBox_year = new QComboBox(ScholarShip);
        comboBox_year->setObjectName(QString::fromUtf8("comboBox_year"));

        gridLayout->addWidget(comboBox_year, 3, 1, 1, 1);

        pushButton_add2 = new QPushButton(ScholarShip);
        pushButton_add2->setObjectName(QString::fromUtf8("pushButton_add2"));
        pushButton_add2->setMinimumSize(QSize(25, 25));
        pushButton_add2->setMaximumSize(QSize(25, 25));
        pushButton_add2->setStyleSheet(QString::fromUtf8("#pushButton_add2{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add2:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add2, 3, 2, 1, 1);

        gridLayout->setColumnStretch(0, 2);

        verticalLayout->addLayout(gridLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        pushButton = new QPushButton(ScholarShip);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout_2);

        verticalLayout->setStretch(0, 2);

        retranslateUi(ScholarShip);

        QMetaObject::connectSlotsByName(ScholarShip);
    } // setupUi

    void retranslateUi(QDialog *ScholarShip)
    {
        ScholarShip->setWindowTitle(QCoreApplication::translate("ScholarShip", "Dialog", nullptr));
        label_3->setText(QCoreApplication::translate("ScholarShip", "\346\227\245\346\234\237", nullptr));
        label_4->setText(QCoreApplication::translate("ScholarShip", "\345\255\246\346\234\237", nullptr));
        label_2->setText(QCoreApplication::translate("ScholarShip", "\351\207\221\351\242\235", nullptr));
        pushButton_add1->setText(QString());
        label_5->setText(QCoreApplication::translate("ScholarShip", "\345\271\264\344\273\275", nullptr));
        checkBox->setText(QString());
        pushButton_add2->setText(QString());
        pushButton->setText(QCoreApplication::translate("ScholarShip", "\347\241\256\350\256\244", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ScholarShip: public Ui_ScholarShip {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCHOLARSHIP_H
