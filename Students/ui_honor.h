/********************************************************************************
** Form generated from reading UI file 'honor.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HONOR_H
#define UI_HONOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Honor
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_5;
    QLabel *label;
    QLabel *label_4;
    QLabel *label_3;
    QLineEdit *lineEdit_level;
    QComboBox *comboBox_semester;
    QLabel *label_2;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QCheckBox *checkBox;
    QDateEdit *dateEdit;
    QComboBox *comboBox_year;
    QPushButton *pushButton_add1;
    QPushButton *pushButton_add2;
    QLineEdit *lineEdit_name;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;

    void setupUi(QDialog *Honor)
    {
        if (Honor->objectName().isEmpty())
            Honor->setObjectName(QString::fromUtf8("Honor"));
        Honor->resize(303, 234);
        verticalLayout = new QVBoxLayout(Honor);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(-1, -1, 0, -1);
        label_5 = new QLabel(Honor);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 4, 0, 1, 1, Qt::AlignHCenter);

        label = new QLabel(Honor);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1, Qt::AlignHCenter);

        label_4 = new QLabel(Honor);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1, Qt::AlignHCenter);

        label_3 = new QLabel(Honor);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1, Qt::AlignHCenter);

        lineEdit_level = new QLineEdit(Honor);
        lineEdit_level->setObjectName(QString::fromUtf8("lineEdit_level"));

        gridLayout->addWidget(lineEdit_level, 1, 1, 1, 1);

        comboBox_semester = new QComboBox(Honor);
        comboBox_semester->setObjectName(QString::fromUtf8("comboBox_semester"));

        gridLayout->addWidget(comboBox_semester, 3, 1, 1, 1);

        label_2 = new QLabel(Honor);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1, Qt::AlignHCenter);

        widget = new QWidget(Honor);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setLayoutDirection(Qt::LeftToRight);
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(3);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        checkBox = new QCheckBox(widget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        horizontalLayout->addWidget(checkBox);

        dateEdit = new QDateEdit(widget);
        dateEdit->setObjectName(QString::fromUtf8("dateEdit"));

        horizontalLayout->addWidget(dateEdit);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 7);

        gridLayout->addWidget(widget, 2, 1, 1, 1);

        comboBox_year = new QComboBox(Honor);
        comboBox_year->setObjectName(QString::fromUtf8("comboBox_year"));

        gridLayout->addWidget(comboBox_year, 4, 1, 1, 1);

        pushButton_add1 = new QPushButton(Honor);
        pushButton_add1->setObjectName(QString::fromUtf8("pushButton_add1"));
        pushButton_add1->setMinimumSize(QSize(25, 25));
        pushButton_add1->setMaximumSize(QSize(25, 25));
        pushButton_add1->setStyleSheet(QString::fromUtf8("#pushButton_add1{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add1:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add1, 3, 2, 1, 1);

        pushButton_add2 = new QPushButton(Honor);
        pushButton_add2->setObjectName(QString::fromUtf8("pushButton_add2"));
        pushButton_add2->setMinimumSize(QSize(25, 25));
        pushButton_add2->setMaximumSize(QSize(25, 25));
        pushButton_add2->setStyleSheet(QString::fromUtf8("#pushButton_add2{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add2:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add2, 4, 2, 1, 1);

        lineEdit_name = new QLineEdit(Honor);
        lineEdit_name->setObjectName(QString::fromUtf8("lineEdit_name"));

        gridLayout->addWidget(lineEdit_name, 0, 1, 1, 1);

        gridLayout->setRowStretch(0, 1);
        gridLayout->setRowStretch(1, 1);
        gridLayout->setRowStretch(2, 1);
        gridLayout->setRowStretch(3, 1);
        gridLayout->setRowStretch(4, 1);
        gridLayout->setColumnStretch(0, 2);
        gridLayout->setColumnStretch(1, 4);
        gridLayout->setColumnStretch(2, 1);

        verticalLayout->addLayout(gridLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        pushButton = new QPushButton(Honor);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(Honor);

        QMetaObject::connectSlotsByName(Honor);
    } // setupUi

    void retranslateUi(QDialog *Honor)
    {
        Honor->setWindowTitle(QCoreApplication::translate("Honor", "Dialog", nullptr));
        label_5->setText(QCoreApplication::translate("Honor", "\345\271\264\344\273\275", nullptr));
        label->setText(QCoreApplication::translate("Honor", "\345\220\215\347\247\260", nullptr));
        label_4->setText(QCoreApplication::translate("Honor", "\345\255\246\346\234\237", nullptr));
        label_3->setText(QCoreApplication::translate("Honor", "\346\227\245\346\234\237", nullptr));
        label_2->setText(QCoreApplication::translate("Honor", "\347\272\247\345\210\253", nullptr));
        checkBox->setText(QString());
        pushButton_add1->setText(QString());
        pushButton_add2->setText(QString());
        pushButton->setText(QCoreApplication::translate("Honor", "\347\241\256\350\256\244", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Honor: public Ui_Honor {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HONOR_H
