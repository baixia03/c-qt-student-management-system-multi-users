/********************************************************************************
** Form generated from reading UI file 'debt.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEBT_H
#define UI_DEBT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Debt
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label;
    QLabel *label_4;
    QLineEdit *lineEdit_grade;
    QComboBox *comboBox_subject;
    QComboBox *comboBox_year;
    QPushButton *pushButton_add4;
    QPushButton *pushButton_add3;
    QPushButton *pushButton_add1;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QCheckBox *checkBox;
    QDateEdit *dateEdit;
    QComboBox *comboBox_semester;
    QLabel *label_6;
    QComboBox *comboBox_type;
    QPushButton *pushButton_add2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;

    void setupUi(QDialog *Debt)
    {
        if (Debt->objectName().isEmpty())
            Debt->setObjectName(QString::fromUtf8("Debt"));
        Debt->resize(333, 237);
        verticalLayout = new QVBoxLayout(Debt);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(-1, -1, 0, -1);
        label_2 = new QLabel(Debt);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1, Qt::AlignHCenter);

        label_3 = new QLabel(Debt);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 3, 0, 1, 1, Qt::AlignHCenter);

        label_5 = new QLabel(Debt);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 5, 0, 1, 1, Qt::AlignHCenter);

        label = new QLabel(Debt);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1, Qt::AlignHCenter);

        label_4 = new QLabel(Debt);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 4, 0, 1, 1, Qt::AlignHCenter);

        lineEdit_grade = new QLineEdit(Debt);
        lineEdit_grade->setObjectName(QString::fromUtf8("lineEdit_grade"));

        gridLayout->addWidget(lineEdit_grade, 1, 1, 1, 1);

        comboBox_subject = new QComboBox(Debt);
        comboBox_subject->setObjectName(QString::fromUtf8("comboBox_subject"));

        gridLayout->addWidget(comboBox_subject, 0, 1, 1, 1);

        comboBox_year = new QComboBox(Debt);
        comboBox_year->setObjectName(QString::fromUtf8("comboBox_year"));

        gridLayout->addWidget(comboBox_year, 5, 1, 1, 1);

        pushButton_add4 = new QPushButton(Debt);
        pushButton_add4->setObjectName(QString::fromUtf8("pushButton_add4"));
        pushButton_add4->setMinimumSize(QSize(25, 25));
        pushButton_add4->setMaximumSize(QSize(25, 25));
        pushButton_add4->setStyleSheet(QString::fromUtf8("#pushButton_add4{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add4:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add4, 5, 2, 1, 1);

        pushButton_add3 = new QPushButton(Debt);
        pushButton_add3->setObjectName(QString::fromUtf8("pushButton_add3"));
        pushButton_add3->setMinimumSize(QSize(25, 25));
        pushButton_add3->setMaximumSize(QSize(25, 25));
        pushButton_add3->setStyleSheet(QString::fromUtf8("#pushButton_add3{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add3:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add3, 4, 2, 1, 1);

        pushButton_add1 = new QPushButton(Debt);
        pushButton_add1->setObjectName(QString::fromUtf8("pushButton_add1"));
        pushButton_add1->setMinimumSize(QSize(25, 25));
        pushButton_add1->setMaximumSize(QSize(25, 25));
        pushButton_add1->setStyleSheet(QString::fromUtf8("#pushButton_add1{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add1:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add1, 0, 2, 1, 1);

        widget = new QWidget(Debt);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setLayoutDirection(Qt::LeftToRight);
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(3);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        checkBox = new QCheckBox(widget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));

        horizontalLayout->addWidget(checkBox);

        dateEdit = new QDateEdit(widget);
        dateEdit->setObjectName(QString::fromUtf8("dateEdit"));

        horizontalLayout->addWidget(dateEdit);

        horizontalLayout->setStretch(0, 1);
        horizontalLayout->setStretch(1, 7);

        gridLayout->addWidget(widget, 3, 1, 1, 1);

        comboBox_semester = new QComboBox(Debt);
        comboBox_semester->setObjectName(QString::fromUtf8("comboBox_semester"));

        gridLayout->addWidget(comboBox_semester, 4, 1, 1, 1);

        label_6 = new QLabel(Debt);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 2, 0, 1, 1, Qt::AlignHCenter);

        comboBox_type = new QComboBox(Debt);
        comboBox_type->setObjectName(QString::fromUtf8("comboBox_type"));

        gridLayout->addWidget(comboBox_type, 2, 1, 1, 1);

        pushButton_add2 = new QPushButton(Debt);
        pushButton_add2->setObjectName(QString::fromUtf8("pushButton_add2"));
        pushButton_add2->setMinimumSize(QSize(25, 25));
        pushButton_add2->setMaximumSize(QSize(25, 25));
        pushButton_add2->setStyleSheet(QString::fromUtf8("#pushButton_add2{\n"
"Border-image:url(:/images/add.png);\n"
"}\n"
"#pushButton_add2:pressed{\n"
"border: 1px solid rgb(24, 103, 155);\n"
"border-radius: 5px;\n"
"background-color: rgb(124, 203, 255);\n"
"color: white;\n"
"}"));

        gridLayout->addWidget(pushButton_add2, 2, 2, 1, 1);

        gridLayout->setRowStretch(0, 1);
        gridLayout->setRowStretch(1, 1);
        gridLayout->setRowStretch(2, 1);
        gridLayout->setRowStretch(3, 1);
        gridLayout->setRowStretch(4, 1);
        gridLayout->setColumnStretch(0, 2);
        gridLayout->setColumnStretch(1, 4);
        gridLayout->setColumnStretch(2, 1);

        verticalLayout->addLayout(gridLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        pushButton = new QPushButton(Debt);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(Debt);

        QMetaObject::connectSlotsByName(Debt);
    } // setupUi

    void retranslateUi(QDialog *Debt)
    {
        Debt->setWindowTitle(QCoreApplication::translate("Debt", "Dialog", nullptr));
        label_2->setText(QCoreApplication::translate("Debt", "\345\210\206\346\225\260", nullptr));
        label_3->setText(QCoreApplication::translate("Debt", "\346\227\245\346\234\237", nullptr));
        label_5->setText(QCoreApplication::translate("Debt", "\345\271\264\344\273\275", nullptr));
        label->setText(QCoreApplication::translate("Debt", "\347\247\221\347\233\256", nullptr));
        label_4->setText(QCoreApplication::translate("Debt", "\345\255\246\346\234\237", nullptr));
        pushButton_add4->setText(QString());
        pushButton_add3->setText(QString());
        pushButton_add1->setText(QString());
        checkBox->setText(QString());
        label_6->setText(QCoreApplication::translate("Debt", "\347\261\273\345\236\213", nullptr));
        pushButton_add2->setText(QString());
        pushButton->setText(QCoreApplication::translate("Debt", "\347\241\256\350\256\244", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Debt: public Ui_Debt {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEBT_H
