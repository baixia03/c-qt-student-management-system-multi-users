/********************************************************************************
** Form generated from reading UI file 'datadictionary.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATADICTIONARY_H
#define UI_DATADICTIONARY_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DataDictionary
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *tab_17;
    QHBoxLayout *horizontalLayout_4;
    QListWidget *listWidget_nation;
    QVBoxLayout *verticalLayout_3;
    QPushButton *pushButton_addNation;
    QPushButton *pushButton_editNation;
    QPushButton *pushButton_deleteNation;
    QSpacerItem *verticalSpacer_3;
    QWidget *tab_16;
    QHBoxLayout *horizontalLayout_5;
    QListWidget *listWidget_political;
    QVBoxLayout *verticalLayout_4;
    QPushButton *pushButton_addPolitical;
    QPushButton *pushButton_editPolitical;
    QPushButton *pushButton_deletePolitical;
    QSpacerItem *verticalSpacer_4;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_2;
    QListWidget *listWidget_province;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_addProvince;
    QPushButton *pushButton_editProvince;
    QPushButton *pushButton_deleteProvince;
    QSpacerItem *verticalSpacer;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_6;
    QListWidget *listWidget_city;
    QVBoxLayout *verticalLayout_5;
    QPushButton *pushButton_addCity;
    QPushButton *pushButton_editCity;
    QPushButton *pushButton_deleteCity;
    QSpacerItem *verticalSpacer_5;
    QWidget *tab_3;
    QHBoxLayout *horizontalLayout_7;
    QListWidget *listWidget_university;
    QVBoxLayout *verticalLayout_6;
    QPushButton *pushButton_addUniversity;
    QPushButton *pushButton_editUniversity;
    QPushButton *pushButton_deleteUniversity;
    QSpacerItem *verticalSpacer_6;
    QWidget *tab_4;
    QHBoxLayout *horizontalLayout_8;
    QListWidget *listWidget_college;
    QVBoxLayout *verticalLayout_7;
    QPushButton *pushButton_addCollege;
    QPushButton *pushButton_editCollege;
    QPushButton *pushButton_deleteCollege;
    QSpacerItem *verticalSpacer_7;
    QWidget *tab_5;
    QHBoxLayout *horizontalLayout_9;
    QListWidget *listWidget_profession;
    QVBoxLayout *verticalLayout_8;
    QPushButton *pushButton_addProfession;
    QPushButton *pushButton_editProfession;
    QPushButton *pushButton_deleteProfession;
    QSpacerItem *verticalSpacer_8;
    QWidget *tab_6;
    QHBoxLayout *horizontalLayout_10;
    QListWidget *listWidget_status;
    QVBoxLayout *verticalLayout_9;
    QPushButton *pushButton_addStatus;
    QPushButton *pushButton_editStatus;
    QPushButton *pushButton_deleteStatus;
    QSpacerItem *verticalSpacer_9;
    QWidget *tab_7;
    QHBoxLayout *horizontalLayout_11;
    QListWidget *listWidget_socialStatus;
    QVBoxLayout *verticalLayout_10;
    QPushButton *pushButton_addSocialStatus;
    QPushButton *pushButton_editSocialStatus;
    QPushButton *pushButton_deleteSocialStatus;
    QSpacerItem *verticalSpacer_10;
    QWidget *tab_8;
    QHBoxLayout *horizontalLayout_12;
    QListWidget *listWidget_blood;
    QVBoxLayout *verticalLayout_11;
    QPushButton *pushButton_addBlood;
    QPushButton *pushButton_editBlood;
    QPushButton *pushButton_deleteBlood;
    QSpacerItem *verticalSpacer_11;
    QWidget *tab_9;
    QHBoxLayout *horizontalLayout_13;
    QListWidget *listWidget_eye;
    QVBoxLayout *verticalLayout_12;
    QPushButton *pushButton_addEye;
    QPushButton *pushButton_editEye;
    QPushButton *pushButton_deleteEye;
    QSpacerItem *verticalSpacer_12;
    QWidget *tab_10;
    QHBoxLayout *horizontalLayout_14;
    QListWidget *listWidget_skin;
    QVBoxLayout *verticalLayout_13;
    QPushButton *pushButton_addSkin;
    QPushButton *pushButton_editSkin;
    QPushButton *pushButton_deleteSkin;
    QSpacerItem *verticalSpacer_13;
    QWidget *tab_11;
    QHBoxLayout *horizontalLayout_3;
    QListWidget *listWidget_subject;
    QVBoxLayout *verticalLayout_14;
    QPushButton *pushButton_addSubject;
    QPushButton *pushButton_editSubject;
    QPushButton *pushButton_deleteSubject;
    QSpacerItem *verticalSpacer_14;
    QWidget *tab_12;
    QHBoxLayout *horizontalLayout_15;
    QListWidget *listWidget_year;
    QVBoxLayout *verticalLayout_15;
    QPushButton *pushButton_addYear;
    QPushButton *pushButton_editYear;
    QPushButton *pushButton_deleteYear;
    QSpacerItem *verticalSpacer_15;
    QWidget *tab_13;
    QHBoxLayout *horizontalLayout_16;
    QListWidget *listWidget_semester;
    QVBoxLayout *verticalLayout_16;
    QPushButton *pushButton_addSemester;
    QPushButton *pushButton_editSemester;
    QPushButton *pushButton_deleteSemester;
    QSpacerItem *verticalSpacer_16;
    QWidget *tab_14;
    QHBoxLayout *horizontalLayout_17;
    QListWidget *listWidget_typeOfExam;
    QVBoxLayout *verticalLayout_17;
    QPushButton *pushButton_addTypeOfExam;
    QPushButton *pushButton_editTypeOfExam;
    QPushButton *pushButton_deleteTypeOfExam;
    QSpacerItem *verticalSpacer_17;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *DataDictionary)
    {
        if (DataDictionary->objectName().isEmpty())
            DataDictionary->setObjectName(QString::fromUtf8("DataDictionary"));
        DataDictionary->resize(1139, 645);
        centralwidget = new QWidget(DataDictionary);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab_17 = new QWidget();
        tab_17->setObjectName(QString::fromUtf8("tab_17"));
        horizontalLayout_4 = new QHBoxLayout(tab_17);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        listWidget_nation = new QListWidget(tab_17);
        listWidget_nation->setObjectName(QString::fromUtf8("listWidget_nation"));

        horizontalLayout_4->addWidget(listWidget_nation);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        pushButton_addNation = new QPushButton(tab_17);
        pushButton_addNation->setObjectName(QString::fromUtf8("pushButton_addNation"));

        verticalLayout_3->addWidget(pushButton_addNation);

        pushButton_editNation = new QPushButton(tab_17);
        pushButton_editNation->setObjectName(QString::fromUtf8("pushButton_editNation"));

        verticalLayout_3->addWidget(pushButton_editNation);

        pushButton_deleteNation = new QPushButton(tab_17);
        pushButton_deleteNation->setObjectName(QString::fromUtf8("pushButton_deleteNation"));

        verticalLayout_3->addWidget(pushButton_deleteNation);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_3);


        horizontalLayout_4->addLayout(verticalLayout_3);

        tabWidget->addTab(tab_17, QString());
        tab_16 = new QWidget();
        tab_16->setObjectName(QString::fromUtf8("tab_16"));
        horizontalLayout_5 = new QHBoxLayout(tab_16);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        listWidget_political = new QListWidget(tab_16);
        listWidget_political->setObjectName(QString::fromUtf8("listWidget_political"));

        horizontalLayout_5->addWidget(listWidget_political);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        pushButton_addPolitical = new QPushButton(tab_16);
        pushButton_addPolitical->setObjectName(QString::fromUtf8("pushButton_addPolitical"));

        verticalLayout_4->addWidget(pushButton_addPolitical);

        pushButton_editPolitical = new QPushButton(tab_16);
        pushButton_editPolitical->setObjectName(QString::fromUtf8("pushButton_editPolitical"));

        verticalLayout_4->addWidget(pushButton_editPolitical);

        pushButton_deletePolitical = new QPushButton(tab_16);
        pushButton_deletePolitical->setObjectName(QString::fromUtf8("pushButton_deletePolitical"));

        verticalLayout_4->addWidget(pushButton_deletePolitical);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_4);


        horizontalLayout_5->addLayout(verticalLayout_4);

        tabWidget->addTab(tab_16, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        horizontalLayout_2 = new QHBoxLayout(tab);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        listWidget_province = new QListWidget(tab);
        listWidget_province->setObjectName(QString::fromUtf8("listWidget_province"));

        horizontalLayout_2->addWidget(listWidget_province);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        pushButton_addProvince = new QPushButton(tab);
        pushButton_addProvince->setObjectName(QString::fromUtf8("pushButton_addProvince"));

        verticalLayout->addWidget(pushButton_addProvince);

        pushButton_editProvince = new QPushButton(tab);
        pushButton_editProvince->setObjectName(QString::fromUtf8("pushButton_editProvince"));

        verticalLayout->addWidget(pushButton_editProvince);

        pushButton_deleteProvince = new QPushButton(tab);
        pushButton_deleteProvince->setObjectName(QString::fromUtf8("pushButton_deleteProvince"));

        verticalLayout->addWidget(pushButton_deleteProvince);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_2->addLayout(verticalLayout);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        horizontalLayout_6 = new QHBoxLayout(tab_2);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        listWidget_city = new QListWidget(tab_2);
        listWidget_city->setObjectName(QString::fromUtf8("listWidget_city"));

        horizontalLayout_6->addWidget(listWidget_city);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        pushButton_addCity = new QPushButton(tab_2);
        pushButton_addCity->setObjectName(QString::fromUtf8("pushButton_addCity"));

        verticalLayout_5->addWidget(pushButton_addCity);

        pushButton_editCity = new QPushButton(tab_2);
        pushButton_editCity->setObjectName(QString::fromUtf8("pushButton_editCity"));

        verticalLayout_5->addWidget(pushButton_editCity);

        pushButton_deleteCity = new QPushButton(tab_2);
        pushButton_deleteCity->setObjectName(QString::fromUtf8("pushButton_deleteCity"));

        verticalLayout_5->addWidget(pushButton_deleteCity);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_5);


        horizontalLayout_6->addLayout(verticalLayout_5);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        horizontalLayout_7 = new QHBoxLayout(tab_3);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        listWidget_university = new QListWidget(tab_3);
        listWidget_university->setObjectName(QString::fromUtf8("listWidget_university"));

        horizontalLayout_7->addWidget(listWidget_university);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        pushButton_addUniversity = new QPushButton(tab_3);
        pushButton_addUniversity->setObjectName(QString::fromUtf8("pushButton_addUniversity"));

        verticalLayout_6->addWidget(pushButton_addUniversity);

        pushButton_editUniversity = new QPushButton(tab_3);
        pushButton_editUniversity->setObjectName(QString::fromUtf8("pushButton_editUniversity"));

        verticalLayout_6->addWidget(pushButton_editUniversity);

        pushButton_deleteUniversity = new QPushButton(tab_3);
        pushButton_deleteUniversity->setObjectName(QString::fromUtf8("pushButton_deleteUniversity"));

        verticalLayout_6->addWidget(pushButton_deleteUniversity);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_6);


        horizontalLayout_7->addLayout(verticalLayout_6);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        horizontalLayout_8 = new QHBoxLayout(tab_4);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        listWidget_college = new QListWidget(tab_4);
        listWidget_college->setObjectName(QString::fromUtf8("listWidget_college"));

        horizontalLayout_8->addWidget(listWidget_college);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        pushButton_addCollege = new QPushButton(tab_4);
        pushButton_addCollege->setObjectName(QString::fromUtf8("pushButton_addCollege"));

        verticalLayout_7->addWidget(pushButton_addCollege);

        pushButton_editCollege = new QPushButton(tab_4);
        pushButton_editCollege->setObjectName(QString::fromUtf8("pushButton_editCollege"));

        verticalLayout_7->addWidget(pushButton_editCollege);

        pushButton_deleteCollege = new QPushButton(tab_4);
        pushButton_deleteCollege->setObjectName(QString::fromUtf8("pushButton_deleteCollege"));

        verticalLayout_7->addWidget(pushButton_deleteCollege);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_7);


        horizontalLayout_8->addLayout(verticalLayout_7);

        tabWidget->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QString::fromUtf8("tab_5"));
        horizontalLayout_9 = new QHBoxLayout(tab_5);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        listWidget_profession = new QListWidget(tab_5);
        listWidget_profession->setObjectName(QString::fromUtf8("listWidget_profession"));

        horizontalLayout_9->addWidget(listWidget_profession);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        pushButton_addProfession = new QPushButton(tab_5);
        pushButton_addProfession->setObjectName(QString::fromUtf8("pushButton_addProfession"));

        verticalLayout_8->addWidget(pushButton_addProfession);

        pushButton_editProfession = new QPushButton(tab_5);
        pushButton_editProfession->setObjectName(QString::fromUtf8("pushButton_editProfession"));

        verticalLayout_8->addWidget(pushButton_editProfession);

        pushButton_deleteProfession = new QPushButton(tab_5);
        pushButton_deleteProfession->setObjectName(QString::fromUtf8("pushButton_deleteProfession"));

        verticalLayout_8->addWidget(pushButton_deleteProfession);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_8->addItem(verticalSpacer_8);


        horizontalLayout_9->addLayout(verticalLayout_8);

        tabWidget->addTab(tab_5, QString());
        tab_6 = new QWidget();
        tab_6->setObjectName(QString::fromUtf8("tab_6"));
        horizontalLayout_10 = new QHBoxLayout(tab_6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        listWidget_status = new QListWidget(tab_6);
        listWidget_status->setObjectName(QString::fromUtf8("listWidget_status"));

        horizontalLayout_10->addWidget(listWidget_status);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        pushButton_addStatus = new QPushButton(tab_6);
        pushButton_addStatus->setObjectName(QString::fromUtf8("pushButton_addStatus"));

        verticalLayout_9->addWidget(pushButton_addStatus);

        pushButton_editStatus = new QPushButton(tab_6);
        pushButton_editStatus->setObjectName(QString::fromUtf8("pushButton_editStatus"));

        verticalLayout_9->addWidget(pushButton_editStatus);

        pushButton_deleteStatus = new QPushButton(tab_6);
        pushButton_deleteStatus->setObjectName(QString::fromUtf8("pushButton_deleteStatus"));

        verticalLayout_9->addWidget(pushButton_deleteStatus);

        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_9);


        horizontalLayout_10->addLayout(verticalLayout_9);

        tabWidget->addTab(tab_6, QString());
        tab_7 = new QWidget();
        tab_7->setObjectName(QString::fromUtf8("tab_7"));
        horizontalLayout_11 = new QHBoxLayout(tab_7);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        listWidget_socialStatus = new QListWidget(tab_7);
        listWidget_socialStatus->setObjectName(QString::fromUtf8("listWidget_socialStatus"));

        horizontalLayout_11->addWidget(listWidget_socialStatus);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        pushButton_addSocialStatus = new QPushButton(tab_7);
        pushButton_addSocialStatus->setObjectName(QString::fromUtf8("pushButton_addSocialStatus"));

        verticalLayout_10->addWidget(pushButton_addSocialStatus);

        pushButton_editSocialStatus = new QPushButton(tab_7);
        pushButton_editSocialStatus->setObjectName(QString::fromUtf8("pushButton_editSocialStatus"));

        verticalLayout_10->addWidget(pushButton_editSocialStatus);

        pushButton_deleteSocialStatus = new QPushButton(tab_7);
        pushButton_deleteSocialStatus->setObjectName(QString::fromUtf8("pushButton_deleteSocialStatus"));

        verticalLayout_10->addWidget(pushButton_deleteSocialStatus);

        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(verticalSpacer_10);


        horizontalLayout_11->addLayout(verticalLayout_10);

        tabWidget->addTab(tab_7, QString());
        tab_8 = new QWidget();
        tab_8->setObjectName(QString::fromUtf8("tab_8"));
        horizontalLayout_12 = new QHBoxLayout(tab_8);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        listWidget_blood = new QListWidget(tab_8);
        listWidget_blood->setObjectName(QString::fromUtf8("listWidget_blood"));

        horizontalLayout_12->addWidget(listWidget_blood);

        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        pushButton_addBlood = new QPushButton(tab_8);
        pushButton_addBlood->setObjectName(QString::fromUtf8("pushButton_addBlood"));

        verticalLayout_11->addWidget(pushButton_addBlood);

        pushButton_editBlood = new QPushButton(tab_8);
        pushButton_editBlood->setObjectName(QString::fromUtf8("pushButton_editBlood"));

        verticalLayout_11->addWidget(pushButton_editBlood);

        pushButton_deleteBlood = new QPushButton(tab_8);
        pushButton_deleteBlood->setObjectName(QString::fromUtf8("pushButton_deleteBlood"));

        verticalLayout_11->addWidget(pushButton_deleteBlood);

        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_11->addItem(verticalSpacer_11);


        horizontalLayout_12->addLayout(verticalLayout_11);

        tabWidget->addTab(tab_8, QString());
        tab_9 = new QWidget();
        tab_9->setObjectName(QString::fromUtf8("tab_9"));
        horizontalLayout_13 = new QHBoxLayout(tab_9);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        listWidget_eye = new QListWidget(tab_9);
        listWidget_eye->setObjectName(QString::fromUtf8("listWidget_eye"));

        horizontalLayout_13->addWidget(listWidget_eye);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        pushButton_addEye = new QPushButton(tab_9);
        pushButton_addEye->setObjectName(QString::fromUtf8("pushButton_addEye"));

        verticalLayout_12->addWidget(pushButton_addEye);

        pushButton_editEye = new QPushButton(tab_9);
        pushButton_editEye->setObjectName(QString::fromUtf8("pushButton_editEye"));

        verticalLayout_12->addWidget(pushButton_editEye);

        pushButton_deleteEye = new QPushButton(tab_9);
        pushButton_deleteEye->setObjectName(QString::fromUtf8("pushButton_deleteEye"));

        verticalLayout_12->addWidget(pushButton_deleteEye);

        verticalSpacer_12 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_12->addItem(verticalSpacer_12);


        horizontalLayout_13->addLayout(verticalLayout_12);

        tabWidget->addTab(tab_9, QString());
        tab_10 = new QWidget();
        tab_10->setObjectName(QString::fromUtf8("tab_10"));
        horizontalLayout_14 = new QHBoxLayout(tab_10);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        listWidget_skin = new QListWidget(tab_10);
        listWidget_skin->setObjectName(QString::fromUtf8("listWidget_skin"));

        horizontalLayout_14->addWidget(listWidget_skin);

        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        pushButton_addSkin = new QPushButton(tab_10);
        pushButton_addSkin->setObjectName(QString::fromUtf8("pushButton_addSkin"));

        verticalLayout_13->addWidget(pushButton_addSkin);

        pushButton_editSkin = new QPushButton(tab_10);
        pushButton_editSkin->setObjectName(QString::fromUtf8("pushButton_editSkin"));

        verticalLayout_13->addWidget(pushButton_editSkin);

        pushButton_deleteSkin = new QPushButton(tab_10);
        pushButton_deleteSkin->setObjectName(QString::fromUtf8("pushButton_deleteSkin"));

        verticalLayout_13->addWidget(pushButton_deleteSkin);

        verticalSpacer_13 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_13->addItem(verticalSpacer_13);


        horizontalLayout_14->addLayout(verticalLayout_13);

        tabWidget->addTab(tab_10, QString());
        tab_11 = new QWidget();
        tab_11->setObjectName(QString::fromUtf8("tab_11"));
        horizontalLayout_3 = new QHBoxLayout(tab_11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        listWidget_subject = new QListWidget(tab_11);
        listWidget_subject->setObjectName(QString::fromUtf8("listWidget_subject"));

        horizontalLayout_3->addWidget(listWidget_subject);

        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        pushButton_addSubject = new QPushButton(tab_11);
        pushButton_addSubject->setObjectName(QString::fromUtf8("pushButton_addSubject"));

        verticalLayout_14->addWidget(pushButton_addSubject);

        pushButton_editSubject = new QPushButton(tab_11);
        pushButton_editSubject->setObjectName(QString::fromUtf8("pushButton_editSubject"));

        verticalLayout_14->addWidget(pushButton_editSubject);

        pushButton_deleteSubject = new QPushButton(tab_11);
        pushButton_deleteSubject->setObjectName(QString::fromUtf8("pushButton_deleteSubject"));

        verticalLayout_14->addWidget(pushButton_deleteSubject);

        verticalSpacer_14 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_14->addItem(verticalSpacer_14);


        horizontalLayout_3->addLayout(verticalLayout_14);

        tabWidget->addTab(tab_11, QString());
        tab_12 = new QWidget();
        tab_12->setObjectName(QString::fromUtf8("tab_12"));
        horizontalLayout_15 = new QHBoxLayout(tab_12);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        listWidget_year = new QListWidget(tab_12);
        listWidget_year->setObjectName(QString::fromUtf8("listWidget_year"));

        horizontalLayout_15->addWidget(listWidget_year);

        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        pushButton_addYear = new QPushButton(tab_12);
        pushButton_addYear->setObjectName(QString::fromUtf8("pushButton_addYear"));

        verticalLayout_15->addWidget(pushButton_addYear);

        pushButton_editYear = new QPushButton(tab_12);
        pushButton_editYear->setObjectName(QString::fromUtf8("pushButton_editYear"));

        verticalLayout_15->addWidget(pushButton_editYear);

        pushButton_deleteYear = new QPushButton(tab_12);
        pushButton_deleteYear->setObjectName(QString::fromUtf8("pushButton_deleteYear"));

        verticalLayout_15->addWidget(pushButton_deleteYear);

        verticalSpacer_15 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_15);


        horizontalLayout_15->addLayout(verticalLayout_15);

        tabWidget->addTab(tab_12, QString());
        tab_13 = new QWidget();
        tab_13->setObjectName(QString::fromUtf8("tab_13"));
        horizontalLayout_16 = new QHBoxLayout(tab_13);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        listWidget_semester = new QListWidget(tab_13);
        listWidget_semester->setObjectName(QString::fromUtf8("listWidget_semester"));

        horizontalLayout_16->addWidget(listWidget_semester);

        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        pushButton_addSemester = new QPushButton(tab_13);
        pushButton_addSemester->setObjectName(QString::fromUtf8("pushButton_addSemester"));

        verticalLayout_16->addWidget(pushButton_addSemester);

        pushButton_editSemester = new QPushButton(tab_13);
        pushButton_editSemester->setObjectName(QString::fromUtf8("pushButton_editSemester"));

        verticalLayout_16->addWidget(pushButton_editSemester);

        pushButton_deleteSemester = new QPushButton(tab_13);
        pushButton_deleteSemester->setObjectName(QString::fromUtf8("pushButton_deleteSemester"));

        verticalLayout_16->addWidget(pushButton_deleteSemester);

        verticalSpacer_16 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_16->addItem(verticalSpacer_16);


        horizontalLayout_16->addLayout(verticalLayout_16);

        tabWidget->addTab(tab_13, QString());
        tab_14 = new QWidget();
        tab_14->setObjectName(QString::fromUtf8("tab_14"));
        horizontalLayout_17 = new QHBoxLayout(tab_14);
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        listWidget_typeOfExam = new QListWidget(tab_14);
        listWidget_typeOfExam->setObjectName(QString::fromUtf8("listWidget_typeOfExam"));

        horizontalLayout_17->addWidget(listWidget_typeOfExam);

        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        pushButton_addTypeOfExam = new QPushButton(tab_14);
        pushButton_addTypeOfExam->setObjectName(QString::fromUtf8("pushButton_addTypeOfExam"));

        verticalLayout_17->addWidget(pushButton_addTypeOfExam);

        pushButton_editTypeOfExam = new QPushButton(tab_14);
        pushButton_editTypeOfExam->setObjectName(QString::fromUtf8("pushButton_editTypeOfExam"));

        verticalLayout_17->addWidget(pushButton_editTypeOfExam);

        pushButton_deleteTypeOfExam = new QPushButton(tab_14);
        pushButton_deleteTypeOfExam->setObjectName(QString::fromUtf8("pushButton_deleteTypeOfExam"));

        verticalLayout_17->addWidget(pushButton_deleteTypeOfExam);

        verticalSpacer_17 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_17->addItem(verticalSpacer_17);


        horizontalLayout_17->addLayout(verticalLayout_17);

        tabWidget->addTab(tab_14, QString());

        horizontalLayout->addWidget(tabWidget);

        DataDictionary->setCentralWidget(centralwidget);
        menubar = new QMenuBar(DataDictionary);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1139, 26));
        DataDictionary->setMenuBar(menubar);
        statusbar = new QStatusBar(DataDictionary);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        DataDictionary->setStatusBar(statusbar);

        retranslateUi(DataDictionary);

        tabWidget->setCurrentIndex(15);


        QMetaObject::connectSlotsByName(DataDictionary);
    } // setupUi

    void retranslateUi(QMainWindow *DataDictionary)
    {
        DataDictionary->setWindowTitle(QCoreApplication::translate("DataDictionary", "MainWindow", nullptr));
        pushButton_addNation->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editNation->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteNation->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_17), QCoreApplication::translate("DataDictionary", "\346\260\221\346\227\217", nullptr));
        pushButton_addPolitical->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editPolitical->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deletePolitical->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_16), QCoreApplication::translate("DataDictionary", "\346\224\277\346\262\273\351\235\242\350\262\214", nullptr));
        pushButton_addProvince->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editProvince->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteProvince->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QCoreApplication::translate("DataDictionary", "\347\234\201\344\273\275", nullptr));
        pushButton_addCity->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editCity->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteCity->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QCoreApplication::translate("DataDictionary", "\345\237\216\345\270\202", nullptr));
        pushButton_addUniversity->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editUniversity->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteUniversity->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QCoreApplication::translate("DataDictionary", "\345\244\247\345\255\246", nullptr));
        pushButton_addCollege->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editCollege->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteCollege->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QCoreApplication::translate("DataDictionary", "\345\255\246\351\231\242", nullptr));
        pushButton_addProfession->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editProfession->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteProfession->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QCoreApplication::translate("DataDictionary", "\344\270\223\344\270\232", nullptr));
        pushButton_addStatus->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editStatus->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteStatus->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_6), QCoreApplication::translate("DataDictionary", "\347\212\266\346\200\201", nullptr));
        pushButton_addSocialStatus->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editSocialStatus->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteSocialStatus->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_7), QCoreApplication::translate("DataDictionary", "\347\244\276\344\272\244\351\200\224\345\276\204", nullptr));
        pushButton_addBlood->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editBlood->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteBlood->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_8), QCoreApplication::translate("DataDictionary", "\350\241\200\345\236\213", nullptr));
        pushButton_addEye->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editEye->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteEye->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_9), QCoreApplication::translate("DataDictionary", " \347\236\263\350\211\262", nullptr));
        pushButton_addSkin->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editSkin->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteSkin->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_10), QCoreApplication::translate("DataDictionary", "\350\202\244\350\211\262", nullptr));
        pushButton_addSubject->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editSubject->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteSubject->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_11), QCoreApplication::translate("DataDictionary", "\347\247\221\347\233\256", nullptr));
        pushButton_addYear->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editYear->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteYear->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_12), QCoreApplication::translate("DataDictionary", "\345\271\264\344\273\275", nullptr));
        pushButton_addSemester->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editSemester->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteSemester->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_13), QCoreApplication::translate("DataDictionary", "\345\255\246\346\234\237", nullptr));
        pushButton_addTypeOfExam->setText(QCoreApplication::translate("DataDictionary", "\346\267\273\345\212\240", nullptr));
        pushButton_editTypeOfExam->setText(QCoreApplication::translate("DataDictionary", "\347\274\226\350\276\221", nullptr));
        pushButton_deleteTypeOfExam->setText(QCoreApplication::translate("DataDictionary", "\345\210\240\351\231\244", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_14), QCoreApplication::translate("DataDictionary", "\350\200\203\350\257\225\347\261\273\345\236\213", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DataDictionary: public Ui_DataDictionary {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATADICTIONARY_H
